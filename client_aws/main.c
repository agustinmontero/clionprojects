#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>
#include <sys/types.h>
#include "main.h"
#include "client_lib.h"

int main() {
    int sockfd, n = -1, intentos = 0;
    long buffer_in_size;
    struct sockaddr_in serv_addr;
    struct hostent *server;
    User *usuario = malloc(sizeof(User));
    Connection *connec = malloc(sizeof(Connection));
    char buffer_out[TAM_OUT], buffer_tamano[LARGE_L];

    while (n < 0 && intentos < MAX_INTENTOS) {
        intentos += 1;
        n = get_usuario(usuario, connec);
        if (n < 0) {
            fprintf(stderr, "Error, uso -> connect nombre_usuario@hostname:port\nIntente nuevamente\n");
        }
    }


    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) {
        perror("ERROR apertura de socket");
        exit(EXIT_FAILURE);
    }

    server = gethostbyname(connec->hostname);
    if (server == NULL) {
        fprintf(stderr, "Error, no existe el host\n");
        exit(EXIT_SUCCESS);
    }
    memset((char *) &serv_addr, '0', sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy(server->h_addr, (char *) &serv_addr.sin_addr.s_addr, (size_t) server->h_length);
    serv_addr.sin_port = htons((uint16_t) connec->port);
    if (connect(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
        perror("conexion");
        exit(EXIT_FAILURE);
    }
    n = login_user(usuario, sockfd);
    if (n < 0) {
        fprintf(stderr, "Usuario y/o contraseña invalida\n");
        exit(EXIT_SUCCESS);
    }
    printf("Conexion establecida\n");

    while (1) {
        memset(buffer_out, '\0', sizeof(buffer_out));
        memset(buffer_tamano, '\0', sizeof(buffer_tamano));

        printf("%s>> ", usuario->nombre);
        fgets(buffer_out, TAM_OUT - 1, stdin);

        if (strtok(strdup(buffer_out), "\n") == NULL) {
            /*Cadena vacía*/
            continue;
        }

        n = (int) write(sockfd, buffer_out, TAM_OUT - 1);
        if (n < 0) {
            perror("escritura de socket");
            exit(EXIT_FAILURE);
        }

        // Verificando si se escribió: fin
        buffer_out[strlen(buffer_out) - 1] = '\0';
        if (!strcmp(FIN, buffer_out)) {
            fprintf(stdout, "Saliendo...\n");
            close(sockfd);
            exit(EXIT_SUCCESS);
        }

        /*Leer tamaño que se va a reservar para el buffer*/
        n = (int) read(sockfd, buffer_tamano, sizeof(char) * LARGE_L);
        if (n < 0) {
            perror("lectura de socket");
            exit(EXIT_FAILURE);
        }
        if (strcmp(buffer_tamano, SERVER_ERROR) == 0) {
            fprintf(stderr, "Error, comando y/o parametro incorrecto.\n");
            memset(buffer_tamano, '\0', LARGE_L);
            continue;
        } else {
            buffer_in_size = atol(buffer_tamano);
            /*printf("\nEl tamaño que se va a reservar para el buffer es %ld\n", buffer_in_size);*/
        }

        if (buffer_in_size == 0) {
            fprintf(stderr, "Error, buffer vacio!\n");
            continue;
        }

        char buffer_in[buffer_in_size];
        memset(buffer_in, '\0', sizeof(buffer_in));

        if (strcmp("descargar", strtok(buffer_out, " ")) == 0) {
            n = remove(FN_DATOS_ESTACION);
            if(n < 0)
                perror("Error al remover archivo ");
            FILE *fp;
            fp = fopen(FN_DATOS_ESTACION, "w+");
            if (fp == NULL) {
                perror("Error al escribir archivo");
                exit(EXIT_FAILURE);
            }
            rewind(fp);
            n = descargar_archivo(buffer_in_size, fp, connec->hostname);
            if(n < 0){
                fprintf(stderr, "Error al descargar archivo.\n");
            } else{
                printf("Descarga exitos, archivo -> %s\n", FN_DATOS_ESTACION);
            }
            fclose(fp);
        } else {
            n = (int) read(sockfd, buffer_in, (sizeof(char) * buffer_in_size));
            if (n < 0) {
                perror("lectura de socket");
                exit(EXIT_FAILURE);
            }
            printf("%s", buffer_in);
            memset(buffer_in, '\0', sizeof(buffer_in));
        }
    }
}

int descargar_archivo(long buffer_in_size, FILE* fp, char* hostname){
    // int bytes_leidos = 0, iter;
    int iter;

    int sockfd, n, tamano_direccion;
	struct sockaddr_in dest_addr;
	struct hostent *server;

	server = gethostbyname(hostname);
	if ( server == NULL ) {
		fprintf( stderr, "ERROR, no existe el host\n");
		return -1;
	}


	sockfd = socket( AF_INET, SOCK_DGRAM, 0 );
	if (sockfd < 0) {
		perror( "Error apertura de socket para descarga " );
		return -1;
	}

	dest_addr.sin_family = AF_INET;
	dest_addr.sin_port = htons(UDP_PORT);
	dest_addr.sin_addr = *( (struct in_addr *)server->h_addr );
	memset( &(dest_addr.sin_zero), '\0', 8 );


	tamano_direccion = sizeof( dest_addr );

    struct timeval read_timeout;
    read_timeout.tv_sec = 6;
    read_timeout.tv_usec = 0;
    setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO, &read_timeout, sizeof read_timeout);

    iter = (int)(buffer_in_size/MAX_UDP_BUF_SIZE);
    if ((buffer_in_size%MAX_UDP_BUF_SIZE) != 0)
        iter += 1;
    // printf("iter=%d\n", iter);

    char buffer_temp[MAX_UDP_BUF_SIZE +1];
    n = sendto( sockfd, "1", 2, 0, (struct sockaddr *)&dest_addr, (socklen_t)tamano_direccion );
	if ( n < 0 ) {
		perror( "Escritura en socket" );
		return -1;
	}
    for (int i = 0; i < iter; i++) {
        memset(buffer_temp, '\0', MAX_UDP_BUF_SIZE + 1);
        n = recvfrom( sockfd, (void *)buffer_temp, MAX_UDP_BUF_SIZE, 0, (struct sockaddr *)&dest_addr, (socklen_t *)&tamano_direccion );
    	if ( n < 0 ) {
    		perror( "Lectura de socket" );
            close(sockfd);
    		return -1;
    	} else if (n == 0) {
            break;
        }

        fprintf(fp, "%s", buffer_temp);
        // bytes_leidos += strlen(buffer_temp);
        // double porc_descarga = (100*bytes_leidos)/buffer_in_size;
        // printf("Descargado\t%.2f - %d bytes recibidos - bytes_leidos=%d\n", porc_descarga, n, bytes_leidos);
    }
    close(sockfd);
    return 0;
}
