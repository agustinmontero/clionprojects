//
// Created by agustin on 03/04/17.
//
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <zconf.h>
#include "client_lib.h"
/// Funcion encargada de calidar el comando inicial 'connect nombre_usuario@host:port:
/// \param usuario estructura User para contener el usuario y password ingresado por el usuario
/// \param connec estructura Connection para guardar el host y port ingresado por el usuario
/// \return -1 en caso de fallo, 0 en caso de exito.
int get_usuario(User *usuario, Connection *connec) {
    char *temp, buffer[MAX_IN];
    fprintf(stdout, ">>");
    fgets(buffer, MAX_IN - 1, stdin);
    if(buffer == NULL){
        fprintf(stderr, "Cadena vacia\n");
        return -1;
    }
    temp = strtok(buffer, " ");
    if(strcmp(temp, "connect") != 0){
        fprintf(stderr, "Comando invalido\n");
        return -1;
    }
    memset(temp, 0 , strlen(temp));
    temp = strtok(NULL, "@");
    if(temp == NULL || (strcmp(temp, "\n") == 0))
        return -1;
    usuario->nombre = strdup(temp);
    memset(temp, 0 , strlen(temp));
    temp = strtok(NULL, ":");
    if(temp == NULL || (strcmp(temp, "\n") == 0))
        return -1;
    connec->hostname = strdup(temp);
    memset(temp, '\0', strlen(temp));
    temp = strtok(NULL, "\n");
    if((temp == NULL) || (strcmp(temp, "\n") == 0))
        return -1;
    connec->port = atoi(temp);

    memset(temp, 0 , strlen(temp));
    memset(buffer, 0, strlen(buffer));
    fprintf(stdout, ">>Password: ");
    fgets(buffer, MAX_IN, stdin);
    if(strcmp(buffer, "\n") == 0)
        return -1;
    usuario->password = strdup(strtok(buffer, "\n"));
    return 0;
}


/// Funcion encargada de loguear a un usuario en el servidor.
/// \param ususario Estructura que contiene el nombre de usuario y password para loguearse en el servidor.
/// \param sockfd descriptor de socket para la comunicacion TCP/IP
/// \return -1 si hubo algun problema, 0 si se pudo loguear con exito el usuario.
int login_user(User *ususario, int sockfd) {
    int n;
    char temp[LARGE_L];
    snprintf(temp, LARGE_L -1, "%s,%s,", ususario->nombre, ususario->password);
    n = (int) write(sockfd, temp, LARGE_L - 1);
    if (n < 0) {
        perror("escritura de socket");
        exit(EXIT_FAILURE);
    }
    memset(temp, '\0', LARGE_L);
    n = (int) read(sockfd, temp, LARGE_L - 1);
    if (n < 0) {
        perror("lectura de socket");
        exit(EXIT_FAILURE);
    }
    if(strcmp(temp, SERVER_ERROR) == 0)
        return -1;
    return 0;
}
