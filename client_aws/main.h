//
// Created by agustin on 03/04/17.
//

#ifndef CLIENT_AWS_MAIN_H
#define CLIENT_AWS_MAIN_H
#define TAM_OUT 256

#define MAX_INTENTOS 4
#define SHORT_L 15
#define MID_L 20
#define LARGE_L 256
#define XL_L 70
#define FIN "desconectar"
#define FN_DATOS_ESTACION "datos_estacion.txt"
#define SERVER_ERROR "-1"
#define UDP_PORT 6040
#define MAX_UDP_BUF_SIZE 512

typedef struct usuario{
    char *nombre;
    char *password;
}User;

typedef struct connection{
    char *hostname;
    int port;
}Connection;

int descargar_archivo(long buffer_in_size, FILE* fp, char* hostname);

#endif //CLIENT_AWS_MAIN_H
