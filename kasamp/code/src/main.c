//
// Created by agustin on 02/09/16.
//
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include "main.h"
#include "part_b.h"
#include "part_c.h"
#include "part_d.h"


int main(int argc, char* argv[]){

    int interval = 0, lapse = 0, c, sflag = 0, lflag = 0;
    opterr = 0;
    while ((c = getopt (argc, argv, "sl:")) != -1){
        switch (c){
            case 's':
                sflag = 1;
                break;
            case 'l':
                lflag = 1;
                interval = atoi(optarg);
                lapse = atoi(argv[optind]);
                break;
            default:
                print_usages();
                exit(EXIT_FAILURE);
        }
    }

    if(sflag & lflag){
        print_usages();
        exit(EXIT_FAILURE);
    }
    else if(sflag ==1) {
        p_print_part_b();
        p_print_part_c();
    } else if(lflag == 1){
        if((interval>0) && (interval<100) && (lapse>interval) & (lapse<1000)){
            p_print_part_d((unsigned int)interval , (unsigned int)lapse);
        } else{
            print_usages();
            exit(EXIT_FAILURE);
        }
    } else{
        p_print_part_b();
    }
    return 0;
}

/**
 * Imprime un mensaje con la sintaxis que se deberia utilizar al ejecutar la aplicacion
 */
void print_usages(void) {
    printf("\t-Parametros invalidos-\n");
    printf("Use:\n\t-s\n\t-l n m\t(int n=intervalo, int m=lapso)\n");
}
