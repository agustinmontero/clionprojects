//
// Created by agustin on 22/09/16.
//

#include <stddef.h>
#include <stdio.h>
#include <string.h>
#include "part_c.h"

/**
 * Funcion encargada de imprimir la informacion requerida por el inciso C: UserTime, SystemTime, IdleTime, Context,
 * BootTime y Processes.
 */
void p_print_part_c(void) {
    printf("UserTime: %s\n", get_cpu_stat((char *) ("user"), (char *) "cpu"));
    printf("SystemTime: %s\n", get_cpu_stat((char *) "system", (char *) "cpu"));
    printf("IdleTime: %s\n", get_cpu_stat((char *) "idle", (char *) "cpu"));
    printf("Context: %s\n", get_cpu_stat(NULL, (char *) "ctxt"));
    printf("BootTime: %s\n", get_cpu_stat(NULL, (char *) "btime"));
    printf("Processes: %s\n", get_cpu_stat(NULL, (char *) "processes"));

}

/**
 *Funcion encargada de obtener la informacion de /proc/stat.
 * @param data String con la columna del cpu que se quiere obtener: user, system o idel. NULL para otros datos.
 * @param line String con el nombre de la linea que se quiere obtener informacion: cpu, ctxt, btime o processes.
 * @return String con la informacion recuperada. NULL si no hubo match.
 */
char *get_cpu_stat(char *data, char *line) {
    struct _IO_FILE * fp;
    char buffer[2048];
    unsigned long bytes_read;
    char* match_block;
    char *string_matched = NULL;//valor que se devuelve

    /* Read the entire contents of /proc/cpuinfo into the buffer.*/
    fp = fopen ("/proc/stat", "r");
    bytes_read = fread (buffer, 1, sizeof (buffer), fp);
    fclose (fp);

    /* Bail if read failed or if buffer isn’t big enough. */
    if (bytes_read == 0 || bytes_read == sizeof (buffer))
        return 0;
    /* NUL-terminate the text. */
    buffer[bytes_read] = '\0';
    /* Locate the line that starts with “cpu”. */

      if(strcmp(line, "cpu") == 0){
        match_block = strstr (buffer, "cpu");
        if (match_block == NULL)
            return 0;
        if(strcmp(data, "user") == 0){
            strtok(match_block, " ");
            string_matched = strtok(NULL, " ");
        }
        else if(strcmp(data, "system") == 0){
            strtok(match_block, " ");
            strtok(NULL, " ");
            strtok(NULL, " ");
            string_matched = strtok(NULL, " ");
        }
        else if(strcmp(data, "idle") == 0){
            strtok(match_block, " ");
            strtok(NULL, " ");
            strtok(NULL, " ");
            strtok(NULL, " ");
            string_matched = strtok(NULL, " ");
        }
    }
    else if(strcmp(line, "ctxt") == 0 ){
        match_block = strstr (buffer, "ctxt");
        if (match_block == NULL)
            return 0;
        strtok(match_block, " ");
        string_matched = strtok(NULL, "\n");
    }
    else if(strcmp(line, "btime") == 0){
        match_block = strstr (buffer, "btime");
        if (match_block == NULL)
            return 0;
        strtok(match_block, " ");
        string_matched = strtok(NULL, "\n");
    }
    else if(strcmp(line, "processes") == 0){
        match_block = strstr (buffer, "processes");
        if (match_block == NULL)
            return 0;
        strtok(match_block, " ");
        string_matched = strtok(NULL, "\n");
    }

    return string_matched;
}
