//
// Created by agustin on 22/09/16.
//
#include <sys/sysinfo.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include "part_d.h"

/**
 *Funcion encargada de imprimir el inciso D(Peticiones a disco Memoria disponible/total y
 * Promedio de carga en el ultimo minuto.
 * @param interval Cantidad en segundo de la duracion del intervalo de repeticion.
 * @param lapse Cantidad total en segundos durante los cuales se debe repetir en pantalla la impresion de la info.
 */
void p_print_part_d(unsigned int interval, unsigned int lapse) {
    int count = 0;
    while(count<lapse){
        count += interval;
        printf("Peticiones a disco: %d\n", get_disk_petitions());
        print_disp_total_ram();
        printf("Promedio de carga en el ultimo minuto:%s\n", get_load_avg());
        printf("Esperando %d seg...\n", interval);
        sleep(interval);
    }

}

/**
 * Imprime la memoria RAM libre y la total
 */
void print_disp_total_ram(void) {
    struct sysinfo si;
    sysinfo (&si);
    printf ("Memoria disponible/total : %ld/%ld \n", si.freeram ,si.totalram);
}

/**
 *
 * @return String con el el promedio de carga en el ultimo minuto
 */
char * get_load_avg(void) {
    FILE *fp;
    size_t nbytes = 100;
    char* load_avg_str;

    fp = fopen ("/proc/loadavg", "r");
    load_avg_str = (char *) malloc(nbytes+ 1);
    getline(&load_avg_str, &nbytes, fp);
    fclose(fp);
    strtok(load_avg_str, " ");//corta la cadena cuando hay un espacio
    return load_avg_str;
}

/**
 *
 * @return Devuelve un entero con la cantidad de Peticiones al disco(lectrua + escritura)
 */
int get_disk_petitions() {
    FILE* fp;
    char buffer[3072];
    size_t bytes_read;
    char* match_block;
    int reads_completed = 0, writes_completed = 0, total = 0;

    fp = fopen ("/proc/diskstats", "r");
    bytes_read = fread (buffer, 1, sizeof (buffer), fp);
    fclose (fp);

    if (bytes_read == 0 || bytes_read == sizeof (buffer))
        return 0;
    /* NUL-terminate the text. */
    buffer[bytes_read] = '\0';
    /* Locate the line that starts with “data”. */
    match_block = strstr (buffer, "sda");
    if (match_block == NULL)
        return 0;
    strtok(match_block, " ");
    reads_completed = atoi(strtok(NULL, " "));
    for (int i = 0; i < 3; ++i) {
        strtok(NULL, " ");
    }
    writes_completed = atoi(strtok(NULL, " "));
    total = reads_completed + writes_completed;
    return total;
}


