//
// Created by agustin on 22/09/16.
//

#include <wchar.h>
#include <stdio.h>
#include <string.h>
#include <sys/time.h>
#include <time.h>
#include <stdlib.h>
#include "part_b.h"

/**
 * Funcion encargada de imprimir en pantalla la informacion requerida en el inciso B.
 */
void p_print_part_b(void) {
    int* up_time;
    printf("Hotsname: %s", get_hostname());
    print_time();
    printf ("Type: %s \n", get_cpu_info("vendor_id"));
    printf ("Model: %s \n", get_cpu_info("model name"));
    up_time = get_up_time();
    printf("Up Time: %d D ", *(up_time));
    printf("%d:", *(up_time+1));
    printf("%d:", *(up_time+2));
    printf("%d\n", *(up_time+3));
    printf("FileSystem: %d\n", get_filesystem_count());

}

/**
 * Funcion para recuperar informacion de /proc/cpuinfo
 * @param data String con el campo que se desea obtener.
 * @return String con la informacion del CPU solicitada. NULL si no hubo match.
 */
char *get_cpu_info(char *data) {
    FILE* fp;
    char buffer[2048];
    size_t bytes_read;
    char* match_block;
    char *string_matched = NULL;//valor que se devuelve

    /* Read the entire contents of /proc/cpuinfo into the buffer.*/
    fp = fopen ("/proc/cpuinfo", "r");
    bytes_read = fread (buffer, 1, sizeof (buffer), fp);
    fclose (fp);

    /* Bail if read failed or if buffer isn’t big enough. */
    if (bytes_read == 0 || bytes_read == sizeof (buffer))
        return 0;
    /* NUL-terminate the text. */
    buffer[bytes_read] = '\0';
    /* Locate the line that starts with “data”. */
    match_block = strstr (buffer, data);
    if (match_block == NULL)
        return 0;

    /* Parse the line to extract the data. */
    strtok(match_block, ":");
    string_matched = strtok(NULL, "\n");
    return string_matched;
}

/**
 * Imprime la fecha y hora actuales en el formato: yyyy-mm-dd hh:mm:ss.
 */
void print_time(void) {
    struct timeval tv;
    struct tm* ptm;
    char time_string[40];
    long milliseconds;
    /* Obtain the time of day, and convert it to a tm struct. */
    gettimeofday (&tv, NULL);
    ptm = localtime (&tv.tv_sec);
    /* Format the date and time, down to a single second. */
    strftime (time_string, sizeof (time_string), "%Y-%m-%d %H:%M:%S", ptm);
    /* Compute milliseconds from microseconds. */
    milliseconds = tv.tv_usec / 1000;
    /* Print the formatted time, in seconds, followed by a decimal point
    and the milliseconds. */
    printf ("%s.%03ld\n", time_string, milliseconds);

}

/**
 * Recupera la informacion de /proc/uptime.
 * @return Puntero al primer elemento de un arreglo cuyos elementos son: [0] = days, [1] = hours, [2] = min, [3] = sec.
 */
int *get_up_time() {
    size_t nbytes = 100;
    FILE* fp;
    char* my_string;

    fp = fopen ("/proc/uptime", "r");
    my_string = (char *) malloc(nbytes+ 1);
    getline(&my_string, &nbytes, fp);
    fclose(fp);

    strtok(my_string, " ");//corta la cadena cuando hay un espacio

    return format_time(my_string);
}

/**
 *
 * @param segundos String con los segundos a formatear en la forma D h:m:s
 * @return Puntero al primer elemento de un arreglo que contiene: [0] = days, [1] = hours, [2] = min, [3] = sec.
 */
int *format_time(char *segundos) {
    int t, days=0, hours=0, min=0, sec=0, m=0;
    const int DAY = 86400, HOUR = 3600, MINUTE = 60;
    static int time[4];
    t = atoi(segundos);
    if(t>=DAY){
        m = t%DAY;
        days = (t - m) / DAY;
        t -= days*DAY;
    }
    if(t>=HOUR){
        m = t%HOUR;
        hours = (t-m)/HOUR;
        t -= hours*HOUR;
    }
    if(t>=MINUTE){
        m = t%MINUTE;
        min = (t-m)/MINUTE;
        t -= min*MINUTE;
    }
    sec = t;
    time[0] = days, time[1] = hours, time[2] = min, time[3] = sec;
    return time;
}

/**
 * @return Cantidad de sistemas de archivo soportados por el kernel que figuran en /proc/filesystem
 */
int get_filesystem_count() {
    FILE *fp;
    uint counter = 0;
    size_t b = 30;
    char *c;

    fp = fopen ("/proc/filesystems", "r");
    while (getline(&c, &b, fp) != EOF){counter += 1;}

    fclose(fp);
    return counter;
}

/**
 * @return string con el nombre del host ubicado en /proc/sys/kernel/hostname
 */
char *get_hostname(void) {
    FILE *fp;
    size_t nbytes = 50;
    char* hostname_str;

    fp = fopen ("/proc/sys/kernel/hostname", "r");
    hostname_str = (char *) malloc(nbytes+ 1);
    getline(&hostname_str, &nbytes, fp);
    fclose(fp);
    return hostname_str;
}
