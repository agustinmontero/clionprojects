//
// Created by agustin on 22/09/16.
//

#ifndef KASAMP_PART_D_H
#define KASAMP_PART_D_H

void p_print_part_d(unsigned int interval, unsigned int lapse);
void print_disp_total_ram(void);
char * get_load_avg(void);
int get_disk_petitions();


#endif //KASAMP_PART_D_H
