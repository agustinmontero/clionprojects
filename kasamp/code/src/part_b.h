//
// Created by agustin on 22/09/16.
//

#ifndef KASAMP_PART_B_H
#define KASAMP_PART_B_H
void p_print_part_b(void);
char *get_hostname(void);
void print_time(void);
char* get_cpu_info(char *data);
int* get_up_time();
int *format_time(char *segundos);
int get_filesystem_count();

#endif //KASAMP_PART_B_H
