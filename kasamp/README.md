#**Sistemas operativos I - Trabajo práctico I**

###Introducción
Ksamp es una aplicación escrtita en C que sirve para recolectar y mostrar una parte de la informacion que se encuentra en el sistema de archivos /proc 
del Kernel de Linux. Al momento la aplicacion cuenta con 3 tipos de ejecucion segun los parametros que se le pasen. En todos los casos la informacion se
muestra a traves de ls standar output.

#####Instrucciones de compilacion
La aplicacion se debe compilar utilizando el comando make dentro del directorio ~/kasamp/code/src/. Luego de finalizar la compilacion se genera un 
ejecutable con el nombre 'ksamp'.

#####Sinopsis
./ksamp [OPTIONS]

#####Descripcion
Muestra informacion especifica del sistema de archivos /proc de Linux. La ejecucion sin parametros de entrada muestra: CPU Type, CPU Model, UpTime
 y FileSystem count.
 `ksamp -s `: Muestra CPU Type, CPU Model, UpTime, FileSystem count, UserTime, SystemTime, IdleTime, Context, BootTime y Processes
 `ksamp -l x y`: Muestra peticiones de disco, cantidades de memoria y  promedios de carga de /proc, y los imprime repetidamente cada "x" segundos; 
 esto se repite hasta que hayan pasado "y" segundos.
                  
 


