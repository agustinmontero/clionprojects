cmake_minimum_required(VERSION 3.6)
project(kasamp)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

set(SOURCE_FILES code/src/main.c code/src/main.h code/src/part_c.c code/src/part_c.h code/src/part_d.c code/src/part_d.h code/src/part_b.c code/src/part_b.h)
add_executable(kasamp ${SOURCE_FILES} code/src/main.c code/src/part_c.c code/src/part_c.h code/src/part_d.c code/src/part_d.h code/src/part_b.c code/src/part_b.h)