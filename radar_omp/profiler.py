from subprocess import check_call, CalledProcessError


COMMAND = '/usr/bin/time -f %e --output=out.txt -a ./main'
prof_command = COMMAND.split(' ')
ITERATIONS = 10

if __name__ == '__main__':
    for i in range(ITERATIONS):
        try:
            result = check_call(prof_command)
        except (CalledProcessError, OSError):
            print("Error al ejecutar main")
