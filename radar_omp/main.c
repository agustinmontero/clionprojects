#include "main.h"

int main() {
    /*struct timeval st, et;
    gettimeofday(&st,NULL);*/
    FILE *fp;
    uint16_t valid_samles = 0;
    int eof, pulsos_count, pulsos_per_gate,total_gates,count = 0, array_len, max_row_len;
    float n1_prob = 0, n_prob = 0;// probabilidad de tener n+1 pulsos y n pulsos respectivamente.
    int n1_ocurr = 0, n_ocurr = 0;// Cuantas veces se deben tomar n+1 pulsos y cuantas n pulsos.
    int n1_count = 0, n_count = 0;// Contadores para cuando se toman n+1 y n pulsos.
    float muestra = 0, rest;
    long offset;
    total_gates = (int) (ALCANCE / GATE);

    fp = fopen("pulsos.iq", "rb");
    if (fp == NULL) {
        perror("Error al abrir archivo");
        exit(EXIT_FAILURE);
    }

    /*************** Asignacion de memoria ********************/
    rewind(fp);
    fseek(fp, 0, SEEK_END);
    eof = (int) ftell(fp);
    array_len = get_array_len(fp, eof, &max_row_len, total_gates);
    //printf("La cantidad de filas para cada matriz sera:%d\n", max_row_len);
    struct canal_v canal_v_array[array_len][max_row_len][total_gates];
    //printf("El tamaño de  canal_v_array=%lu\n", sizeof(canal_v_array));
    struct canal_h canal_h_array[array_len][max_row_len][total_gates];
    //printf("El tamaño de  canal_h_array=%lu\n", sizeof(canal_h_array));
    int8_t cant_pulsos[array_len][total_gates];/*Pulsos que tendra cada gate. formato: [numero_de_matriz][nro_gate]*/
    rewind(fp);

    while (ftell(fp) < eof) {
        pulsos_count = 0;
        fread((void*)(&valid_samles), sizeof(uint16_t),1, fp);
        // printf("La cantidad de muestras es: %u\n", valid_samles);
        rest = valid_samles % total_gates;
        pulsos_per_gate = (int) ((valid_samles - rest) / total_gates);
        n1_prob = (rest/total_gates);
        n_prob = 1 - n1_prob;
        if (n1_prob > n_prob) {
            n_ocurr = 1;
            n1_ocurr = (int) (n1_prob / n_prob);
        } else if (n1_prob < n_prob) {
            n_ocurr = (int) (n_prob / n1_prob);
            n1_ocurr = 1;
        } else if(n1_prob == 0){
            n_ocurr = total_gates;
        } else{
            n_ocurr = 1;
            n1_ocurr = 1;
        }
        // printf("n1_ocurr=%d -- n_ocurr=%d\n", n1_ocurr, n_ocurr);
        // printf("pulsos_per_gate=%d\n", pulsos_per_gate);
        /*Asigancion al canal V, numero_de_matriz=count*/

        for (int j = 0; j < total_gates; ++j) {
            if (n1_count < n1_ocurr) {
                cant_pulsos[count][j] = (int8_t)pulsos_per_gate + 1;
                if (count == 71) {
                    // printf("pulsos_per_gate + 1 = %d\n", pulsos_per_gate + 1);
                }
                for (size_t i = 0; i < (pulsos_per_gate + 1); i++) {
                    /*Se rellenan todas las filas de una columna*/
                    pulsos_count += 1;
                    fread((void*)(&muestra), sizeof(muestra), 1, fp);
                    canal_v_array[count][i][j].i = muestra;
                    fread((void*)(&muestra), sizeof(muestra), 1, fp);
                    canal_v_array[count][i][j].q = muestra;
                }
                n1_count += 1;
            } else{
                cant_pulsos[count][j] = (int8_t)pulsos_per_gate;
                for (size_t i = 0; i < (pulsos_per_gate); i++) {
                    pulsos_count += 1;
                    fread((void*)(&muestra), sizeof(muestra), 1, fp);
                    canal_v_array[count][i][j].i = muestra;
                    fread((void*)(&muestra), sizeof(muestra), 1, fp);
                    canal_v_array[count][i][j].q = muestra;
                }
                n_count += 1;
                if (n_count == n_ocurr) {
                    n_count = 0;
                    n1_count = 0;
                }
            }
        }
        // printf("pulsos_count(V)=%d -- descartados(V)=%d\n", pulsos_count,(valid_samles - pulsos_count));
        offset = sizeof(float)*2*(valid_samles - pulsos_count);
        fseek(fp, offset, SEEK_CUR);// Saltear valid_samles de descarte
        pulsos_count = 0;
        n_count = 0;
        n1_count = 0;
        /*Asigancion al canal H, numero_de_matriz=count*/

        for (int j = 0; j < total_gates; ++j) {
            if (n1_count < n1_ocurr) {
                for (size_t i = 0; i < (pulsos_per_gate + 1); i++) {
                    pulsos_count += 1;
                    fread((void*)(&muestra), sizeof(muestra), 1, fp);
                    canal_h_array[count][i][j].i = muestra;
                    fread((void*)(&muestra), sizeof(muestra), 1, fp);
                    canal_h_array[count][i][j].q = muestra;
                }
                n1_count += 1;
            } else{
                for (size_t i = 0; i < (pulsos_per_gate); i++) {
                    pulsos_count += 1;
                    fread((void*)(&muestra), sizeof(muestra), 1, fp);
                    canal_h_array[count][i][j].i = muestra;
                    fread((void*)(&muestra), sizeof(muestra), 1, fp);
                    canal_h_array[count][i][j].q = muestra;
                }
                n_count += 1;
                if (n_count == n_ocurr) {
                    n_count = 0;
                    n1_count = 0;
                }
            }
        }
        // printf("pulsos_count(H)=%d -- descartados(H)=%d\n", pulsos_count,(valid_samles - pulsos_count));
        offset = sizeof(float)*2*(valid_samles - pulsos_count);
        fseek(fp, offset, SEEK_CUR);// Saltear valid_samles de descarte
        count += 1;
    }
    //printf("Se hicieron %d pasadas\n", count);


    FILE *fp_result = fopen("result.iq", "wb");
    if (fp_result == NULL) {
        perror("Error al abrir archivo");
        exit(EXIT_FAILURE);
    }
    rewind(fp_result);
    calc_correlacion_v(fp_result ,array_len, max_row_len, total_gates, canal_v_array, array_len, total_gates, cant_pulsos);
    calc_correlacion_h(fp_result, array_len, max_row_len, total_gates, canal_h_array, array_len, total_gates, cant_pulsos);

    fclose(fp);
    fclose(fp_result);
    /*gettimeofday(&et,NULL);
    int elapsed = (int) (((et.tv_sec - st.tv_sec) * 1000000) + (et.tv_usec - st.tv_usec));
    printf("Sorting time: %d micro seconds\n",elapsed);*/
    return 0;
}

/**
 * Devuelve la cantidad de veces que aparece un valid_samles en el archivo.
 * Ademas, setea en uno de los parametros(max_row_len) cuale es la longitud de
 * la columna mas grande que se deberia almacenar.
 * @param  fp          Puntero a archivo con los datos.
 * @param  eof         Longitud del archivo.
 * @param  max_row_len Puntero para almacenar la cantidad de filas que debera tener la matriz.
 * @param  total_gates Cantidad total de gates.
 * @return             Cantidad de valid_samles encontrados en el archivo.
 */
int get_array_len(FILE *fp, long eof, int *max_row_len, int total_gates) {
    int count = 0, pulsos_per_gate;
    *max_row_len = 0;
    uint16_t valid_samles;
    long offset;
    rewind(fp);
    while (ftell(fp) < eof){
        count += 1;
        fread((void*)(&valid_samles), sizeof(uint16_t),1, fp);
        pulsos_per_gate = ((valid_samles / total_gates) + 1);
        if(pulsos_per_gate > *max_row_len){
            *max_row_len = pulsos_per_gate;
        }
        offset = sizeof(float)*4*valid_samles;
        fseek(fp, offset, SEEK_CUR); // avanzar hasta proximo valid_samles
    }

    return count;
}

/**
 * Calcula la auto corelacion del canal V.
 * @param fp     Puntero a archivo de resultados.
 * @param arr_l  Dimension x de la matriz que contiene las estructuras canal_h
 * @param f      Dimension y de la matriz que contiene las estructuras canal_h
 * @param c      Dimension z de la matriz que contiene las estructuras canal_h
 * @param [name] Matriz 3D que contiene todas las matrices del canal_v
 * @param x      Dimension x de la matriz cant_pulsos
 * @param z      Dimension z de la matriz cant_pulsos
 * @param [name] Matriz que contiene la cantidad de pulsos de cada Gate.
 */
void calc_correlacion_v(FILE *fp, int arr_l, int f, int c, struct canal_v canal_v_array[arr_l][f][c], int x, int z, int8_t cant_pulsos[x][z]){
    int i, j;
    struct canal_v result_v[arr_l][c];
    uint16_t total_v_results = (uint16_t) (arr_l * c);/*Cantidad de resultados que se van a guardar en el archivo*/
    fwrite((void*)(&total_v_results), sizeof(total_v_results), 1, fp);
#pragma omp parallel
    {
        #pragma omp for collapse(2)
            for (i = 0; i < arr_l; ++i) {
                // printf("Matriz_v: %d\n", i);
                for (j = 0; j < c; ++j) {
                    // printf("Gate: %d\n", j);
                    float total_re = 0;
                    float total_im = 0;
                    int8_t cant_pul = cant_pulsos[i][j];
                    for (int k = 0; k < cant_pul-1; ++k) {
                        total_re += (canal_v_array[i][k][j].i*canal_v_array[i][k+1][j].i)-(canal_v_array[i][k][j].q*canal_v_array[i][k+1][j].q);
                        total_im += (canal_v_array[i][k][j].i*canal_v_array[i][k+1][j].q)+(canal_v_array[i][k][j].q*canal_v_array[i][k+1][j].i);
                    }
                    result_v[i][j].i = total_re/cant_pul;
                    result_v[i][j].q = total_im/cant_pul;
                    //printf("%1.12f +(%1.12f)\n", result_v[i][j].i, result_v[i][j].q);
                }
            }
    };
    for (i = 0; i < arr_l; i++) {
        for (j = 0; j < c; j++) {
            fwrite((void*)(&result_v[i][j].i), sizeof(result_v[i][j].i), 1, fp);
            fwrite((void*)(&result_v[i][j].q), sizeof(result_v[i][j].q), 1, fp);
        }
    }
}

/**
 * Calcula la auto corelacion del canal H.
 * @param fp     Puntero a archivo de resultados.
 * @param arr_l  Dimension x de la matriz que contiene las estructuras canal_h
 * @param f      Dimension y de la matriz que contiene las estructuras canal_h
 * @param c      Dimension z de la matriz que contiene las estructuras canal_h
 * @param [name] Matriz 3D que contiene todas las matrices del canal_h
 * @param x      Dimension x de la matriz cant_pulsos
 * @param z      Dimension z de la matriz cant_pulsos
 * @param [name] Matriz que contiene la cantidad de pulsos de cada Gate.
 */
void calc_correlacion_h(FILE *fp, int arr_l, int f, int c, struct canal_h canal_h_array[arr_l][f][c], int x, int z, int8_t cant_pulsos[x][z]){
    int i, j;
    struct canal_h result_h[arr_l][c];
    uint16_t total_h_results = (uint16_t) (arr_l * c);/*Cantidad de resultados que se van a guardar en el archivo*/
    fwrite((void*)(&total_h_results), sizeof(total_h_results), 1, fp);
#pragma omp parallel
    {
        #pragma omp for collapse(2)
            for (i = 0; i < arr_l; ++i) {
                // printf("Matriz_h: %d\n", i);
                for (j = 0; j < c; ++j) {
                    // printf("Gate: %d\n", j);
                    float total_re = 0;
                    float total_im = 0;
                    int8_t cant_pul = cant_pulsos[i][j];

                    for (int k = 0; k < cant_pul-1; ++k) {
                        total_re += (canal_h_array[i][k][j].i*canal_h_array[i][k+1][j].i)-(canal_h_array[i][k][j].q*canal_h_array[i][k+1][j].q);
                        total_im += (canal_h_array[i][k][j].i*canal_h_array[i][k+1][j].q)+(canal_h_array[i][k][j].q*canal_h_array[i][k+1][j].i);
                    }
                    result_h[i][j].i = total_re/cant_pul;
                    result_h[i][j].q = total_im/cant_pul;
                    // printf("%1.12f +(%1.12f)\n", result_h[i][j].i, result_h[i][j].q);
                }
            }
    };
    for (i = 0; i < arr_l; i++) {
        for (j = 0; j < c; j++) {
            fwrite((void*)(&result_h[i][j].i), sizeof(result_h[i][j].i), 1, fp);
            fwrite((void*)(&result_h[i][j].q), sizeof(result_h[i][j].q), 1, fp);
        }
    }
}
