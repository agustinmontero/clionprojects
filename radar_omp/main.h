//
// Created by agustin on 19/10/17.
//
#ifndef RADAR_OMP_MAIN_H
#define RADAR_OMP_MAIN_H
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <sys/time.h>
#include <omp.h>

#define MAX_PRINT 2
#define GATE 0.5
#define ALCANCE 250

struct canal_v {
    float i;
    float q;
};

struct canal_h {
    float i;
    float q;
};

int get_array_len(FILE *fp, long eof, int *max_row_len, int total_gates);
void imprimir_datos_canal_v(int array_len, int filas, int columnas, struct canal_v canal_v_array[array_len][filas][columnas]);
void imprimir_datos_canal_h(int array_len, int filas, int columnas, struct canal_h canal_h_array[array_len][filas][columnas]);
void calc_correlacion_v(FILE *fp, int arr_l, int f, int c, struct canal_v canal_v_array[arr_l][f][c], int x, int z, int8_t cant_pulsos[x][z]);
void calc_correlacion_h(FILE *fp, int arr_l, int f, int c, struct canal_h canal_h_array[arr_l][f][c], int x, int z, int8_t cant_pulsos[x][z]);

#endif //RADAR_OMP_MAIN_H
