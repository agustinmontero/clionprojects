from subprocess import check_call, CalledProcessError

PROF_FILE_BASE_NAME = 'profile_radar_omp_'
PROF_OUT = 'gmon.out'
APP_NAME = 'main'
COMMAND = ' '.join(('gprof', APP_NAME, PROF_OUT))
ITERATIONS = 10

if __name__ == '__main__':
    for i in range(ITERATIONS):
        try:
            result = check_call(['./main'])
        except (CalledProcessError, OSError):
            print("Error al ejecutar main")
        prof_command = COMMAND.split(' ')
        f = open(''.join((PROF_FILE_BASE_NAME, str(i), '.txt')), 'w')
        try:
            result_nd = check_call(prof_command, stdout=f)
        except (CalledProcessError, OSError):
            print("Error al ejecutar gprof")
        f.close()
