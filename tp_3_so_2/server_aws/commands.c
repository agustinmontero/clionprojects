//
// Created by agustin on 25/03/17.
//

#include <string.h>
#include <stdlib.h>
#include "commands.h"

/// Funcion encargada de buscar las distintas estaciones el archivo de base de datos y almacenar en una estructura los
/// datos que cada una de ellas tiene disponibles y cuales no.
/// \param estaciones Puntero a estructura que servirá para contener los datos disponbles de cada estacion
/// \return Cantidad de estaciones disponibles
int listar(struct information_available *estaciones) {
    FILE *fp;
    fp = fopen(FILE_DATOS_MET, "r");
    if (fp == NULL) {
        perror("Error al abrir el archivo: ");
        return -1;
    }
    rewind(fp);
    int total_read = 0, station_count = 0;
    char buffer_l[LINE_BUFF_SIZE], st_number[MID_L];

    while (fgets(buffer_l, LINE_BUFF_SIZE - 1, fp) != NULL) {
        total_read += 1;
        const char false = '0', true = '1';
        strcpy(st_number, strtok(buffer_l, SEPARADOR));
        for (int i = 0; i < MAX_ESTACIONES; ++i) {
            if (strcmp(estaciones[i].numero, NO_DATA) == 0) {
                /*La estacion no estaba en el arreglo, se agrega la estructura*/
                estaciones[station_count].numero = strdup(st_number);
                strcpy((char *) estaciones[station_count].estacion, strtok(NULL, SEPARADOR));
                strcpy((char *) estaciones[station_count].id_localidad, strtok(NULL, SEPARADOR));
                strcpy((char *) estaciones[station_count].fecha, strtok(NULL, SEPARADOR));
                estaciones[station_count].temperatura = (strcmp(strtok(NULL, SEPARADOR), NO_DATA) == 0 ? false : true);
                estaciones[station_count].humedad = (strcmp(strtok(NULL, SEPARADOR), NO_DATA) == 0 ? false : true);
                estaciones[station_count].pto_de_rocio = (strcmp(strtok(NULL, SEPARADOR), NO_DATA) == 0 ? false : true);
                estaciones[station_count].precipitacion = (strcmp(strtok(NULL, SEPARADOR), NO_DATA) == 0 ? false : true);
                estaciones[station_count].vel_viento = (strcmp(strtok(NULL, SEPARADOR), NO_DATA) == 0 ? false : true);
                estaciones[station_count].direc_viento = (strcmp(strtok(NULL, SEPARADOR), NO_DATA) == 0 ? false : true);
                estaciones[station_count].rafaga_min = (strcmp(strtok(NULL, SEPARADOR), NO_DATA) == 0 ? false : true);
                estaciones[station_count].presion = (strcmp(strtok(NULL, SEPARADOR), NO_DATA) == 0 ? false : true);
                estaciones[station_count].rad_solar = (strcmp(strtok(NULL, SEPARADOR), NO_DATA) == 0 ? false : true);
                estaciones[station_count].temp_suelo_1 = (strcmp(strtok(NULL, SEPARADOR), NO_DATA) == 0 ? false : true);
                estaciones[station_count].temp_suelo_2 = (strcmp(strtok(NULL, SEPARADOR), NO_DATA) == 0 ? false : true);
                estaciones[station_count].temp_suelo_3 = (strcmp(strtok(NULL, SEPARADOR), NO_DATA) == 0 ? false : true);
                estaciones[station_count].humedad_suelo_1 = (strcmp(strtok(NULL, SEPARADOR), NO_DATA) == 0 ? false : true);
                estaciones[station_count].humedad_suelo_2 = (strcmp(strtok(NULL, SEPARADOR), NO_DATA) == 0 ? false : true);
                estaciones[station_count].humedad_suelo_3 = (strcmp(strtok(NULL, SEPARADOR), NO_DATA) == 0 ? false : true);
                estaciones[station_count].humedad_hoja = (strcmp(strtok(NULL, SEPARADOR), NO_DATA) == 0 ? false : true);
                station_count += 1;
                break;
            } else if (strcmp(estaciones[i].numero, st_number) == 0) {
                /*La estacion ya esta agregada, terminar la busequeda*/
                break;
            }
        }
        memset(st_number, '\0', MID_L);
        memset(buffer_l, '\0', LINE_BUFF_SIZE);
    }
    fclose(fp);
    // fprintf(stdout, "Se encontraron %d estaciones en 'listar'. Se leyeron %d lineas\n", station_count, total_read);
    return station_count;
}

/// Funcion encargada de imprimir en un archivo la infromacion formateada de los sensores disponibles de cada estacion.
/// \param buffer_name  nombre de archivo buffer donde se va a escribir la informacion para enviarle al cliente
/// \param estaciones Puntero a estructura que contiene la informacion disponible de cada estacion
/// \param cantidad Es la cantidad de estaciones que hay en la estructura(valor retornado por listar())
/// \return longitud del archivo buffer donde se escribió
long imprimir_estaciones(char *buffer_name, struct information_available *estaciones, int cantidad) {
    FILE *fp_buffer;
    fp_buffer = fopen(buffer_name, "w+");
    if (fp_buffer == NULL) {
        perror("Error al abrir el archuivo buffer: ");
        return -1;
    }
    rewind(fp_buffer);
    const char *datos[19] = {"Nro", "Estacion", "ID Loc", "T[C]", "Hum[HR]", "P.Rocío[C]", "Prec[mm]", "V Vient[Km/h]",
                             "Dir Viento", "Raf Max[Km/h]", "Presión[hPa]", "RadSol[W/m2]", "Temp S. 1[C]",
                             "Temp S 2[C]", "Temp S 3[C]", "HSuelo1[grav]", "HSuelo2[grav]", "HSuelo3[grav]",
                             "H de Hoja[]"};

    const char *disp = "Si", *no_disp = "No";

    for (int j = 0; j < 19; ++j) {
        printf("%7.7s   ", datos[j]);
    }
    printf("\r\n");
    for (int i = 0; i < cantidad; ++i) {
        printf("%7.7s   %7.7s   %7.7s   ", estaciones[i].numero, estaciones[i].estacion, estaciones[i].id_localidad);
        estaciones[i].temperatura == '1' ? printf("%7.7s   ", disp) : printf("%7.7s   ", no_disp);
        estaciones[i].humedad == '1' ? printf("%7.7s   ", disp) : printf("%7.7s   ", no_disp);
        estaciones[i].pto_de_rocio == '1' ? printf("%7.7s   ", disp) : printf("%7.7s   ", no_disp);
        estaciones[i].precipitacion == '1' ? printf("%7.7s   ", disp) : printf("%7.7s   ", no_disp);
        estaciones[i].vel_viento == '1' ? printf("%7.7s   ", disp) : printf("%7.7s   ", no_disp);
        estaciones[i].direc_viento == '1' ? printf("%7.7s   ", disp) : printf("%7.7s   ", no_disp);
        estaciones[i].rafaga_min == '1' ? printf("%7.7s   ", disp) : printf("%7.7s   ", no_disp);
        estaciones[i].presion == '1' ? printf("%7.7s   ", disp) : printf("%7.7s   ", no_disp);
        estaciones[i].rad_solar == '1' ? printf("%7.7s   ", disp) : printf("%7.7s   ", no_disp);
        estaciones[i].temp_suelo_1 == '1' ? printf("%7.7s   ", disp) : printf("%7.7s   ", no_disp);
        estaciones[i].temp_suelo_2 == '1' ? printf("%7.7s   ", disp) : printf("%7.7s   ", no_disp);
        estaciones[i].temp_suelo_3 == '1' ? printf("%7.7s   ", disp) : printf("%7.7s   ", no_disp);
        estaciones[i].humedad_suelo_1 == '1' ? printf("%7.7s   ", disp) : printf("%7.7s   ", no_disp);
        estaciones[i].humedad_suelo_2 == '1' ? printf("%7.7s   ", disp) : printf("%7.7s   ", no_disp);
        estaciones[i].humedad_suelo_3 == '1' ? printf("%7.7s   ", disp) : printf("%7.7s   ", no_disp);
        estaciones[i].humedad_hoja == '1' ? printf("%7.7s   ", disp) : printf("%7.7s   ", no_disp);
        printf("\r\n");
    }
    long pos = ftell(fp_buffer);
    fclose(fp_buffer);
    return pos + 2;
}

/// Guarda toda la informacion de 'numero_estacion' en un archivo.
/// \param numero_estacion El numero de estacion de la que se desea descargar la informacion
/// \return Cantidad de lineas escritas en el archivo
long descargar_nro_estacion(char *numero_estacion, FILE* fp_download_file) {
    FILE *fp;
    fp = fopen(FILE_DATOS_MET, "r");

    if ((fp == NULL)) {
        perror("Error en archivo descargar_nro_estacion)");
        exit(EXIT_FAILURE);
    }
    rewind(fp);

    char buffer_line[LINE_BUFF_SIZE], buffer_numero[XL_L], buff_cp[LINE_BUFF_SIZE];
    int counter = 0;
    memset(buffer_line, '\0', sizeof(buffer_line));
    memset(buffer_numero, '\0', sizeof(buffer_numero));
    memset(buff_cp, '\0', sizeof(buff_cp));

    while (fgets(buffer_line, LINE_BUFF_SIZE, fp) != NULL) {
        strcpy(buff_cp, buffer_line);
        strcpy(buffer_numero, strtok(buffer_line, SEPARADOR));
        if (strcmp(numero_estacion, buffer_numero) == 0) {
            fprintf(fp_download_file, "%s", buff_cp);
            counter += 1;
        }
        memset(buffer_line, '\0', sizeof(buffer_line));
        memset(buffer_numero, '\0', sizeof(buffer_numero));
        memset(buff_cp, '\0', sizeof(buff_cp));
    }
    // fprintf(stdout, "Se escribieron %d lineas en %s linea %d\n", counter, __FILE__, __LINE__);
    fclose(fp);
    return counter;
}


/// Guarda toda la informacion de 'numero_estacion' en un archivo.
/// \param buffer_name nombre de archivo buffer donde se va a guardar la info solicitada
/// \param numero_estacion El numero de estacion de la que se desea descargar la informacion
/// \return Cantidad de lineas escritas en el archivo
long get_estacion(char *buffer_name, char *numero_estacion) {
    FILE *fp, *fp_buffer;
    fp = fopen(FILE_DATOS_MET, "r");
    fp_buffer = fopen(buffer_name, "w+");

    if ((fp == NULL) || (fp_buffer == NULL)) {
        perror("Error en archivo descargar_nro_estacion)");
        exit(EXIT_FAILURE);
    }
    rewind(fp);
    rewind(fp_buffer);

    char buffer_line[LINE_BUFF_SIZE], buffer_numero[XL_L], buff_cp[LINE_BUFF_SIZE];
    int counter = 0;
    memset(buffer_line, '\0', sizeof(buffer_line));
    memset(buffer_numero, '\0', sizeof(buffer_numero));
    memset(buff_cp, '\0', sizeof(buff_cp));

    while (fgets(buffer_line, LINE_BUFF_SIZE, fp) != NULL) {
        strcpy(buff_cp, buffer_line);
        strcpy(buffer_numero, strtok(buffer_line, SEPARADOR));
        if (strcmp(numero_estacion, buffer_numero) == 0) {
            fprintf(fp_buffer, "%s", buff_cp);
            counter += 1;
        }
        memset(buffer_line, '\0', sizeof(buffer_line));
        memset(buffer_numero, '\0', sizeof(buffer_numero));
        memset(buff_cp, '\0', sizeof(buff_cp));
    }
    // fprintf(stdout, "Se escribieron %d lineas en %s linea %d\n", counter, __FILE__, __LINE__);
    fclose(fp);
    long pos = ftell(fp_buffer);
    fclose(fp_buffer);
    return pos + 10;
}


/// Funcion que almacena en una estructura 'precipitacion_diaria' el par de datos (fecha, precipitacion)de una estacion
/// \param buffer_temp_name nombre de archivo que contiene SOLO los datos de la estacion que se pide(archivo de buffer
/// de descargar_nro_estacion)
/// \param prec_dia Puntero a estructura que sirve de almacen para guardar el par (fecha, precipitacion)
/// \return Cantidad de elementos del arreglo prec_dia
int diario_precipitacion(char *buffer_temp_name, struct precipitacion_diaria *prec_dia) {
    // fprintf(stdout, "Entrando a la funcion 'diario_precipitacion' de %s\n", __FILE__);
    FILE *fp_temp;
    fp_temp = fopen(buffer_temp_name, "r");
    if (fp_temp == NULL) {
        perror("Error al abrir buffer temporal");
        return -1;
    }
    rewind(fp_temp);

    char buffer_line[LINE_BUFF_SIZE], buffer_line_fecha[LINE_BUFF_SIZE], fecha_buffer[LARGE_L];
    int count = 0;

    while (fgets(buffer_line, LINE_BUFF_SIZE, fp_temp) != NULL) {
        strcpy(buffer_line_fecha, buffer_line);//Copia de buffer para parseo de fecha y precipitacion
        strtok(buffer_line_fecha, SEPARADOR);
        strtok(NULL, SEPARADOR);
        strtok(NULL, SEPARADOR);
        strcpy(fecha_buffer, strtok(NULL, SEPARADOR));
        strcpy(prec_dia[count].fecha, strtok(fecha_buffer, " "));

        strtok(buffer_line, SEPARADOR);
        for (int j = 0; j < 6; ++j) {
            strtok(NULL, SEPARADOR);
        }
        prec_dia[count].precipitacion = atof(strtok(NULL, SEPARADOR));

        count += 1;
        memset(buffer_line, '\0', sizeof(buffer_line));
        memset(buffer_line_fecha, '\0', sizeof(buffer_line_fecha));
        memset(fecha_buffer, '\0', sizeof(fecha_buffer));
    }
    fclose(fp_temp);
    // fprintf(stdout, "Saliendo de la funcion 'diario_precipitacion' en la linea %d. %d iteraciones\n", __LINE__, count);
    return count - 1;

}

/// Suma todas las muestras de precipitacion de cada dia del mes e imprime el acumulado diario en un archivo.
/// \param buffer_name Puntero a archivo buffer para guardar las precipitaciones acumuladas ya formateadas
/// \param prec_dia puntero a estructura que contiene el par (fecha, precipitacion) de cada medicion de una estacion.
/// Es el mismo puntero que se le paso a diario_precipitacion.
/// \param struct_count cantidad de elementos en el arreglo prec_dia(valor de return de diario_precipitacion)
/// \return
long calcular_acumulado_diario(char *buffer_name, struct precipitacion_diaria *prec_dia, int struct_count) {
    FILE *fp_buffer;
    fp_buffer = fopen(buffer_name, "w+");
    if (fp_buffer == NULL) {
        fprintf(stderr, "Error al abrir archivo de buffer en linea %d de %s\n", __LINE__, __FILE__);
        exit(EXIT_FAILURE);
    }
    rewind(fp_buffer);
    double acumulado = 0;
    char fecha_anterior[MID_L];
    strcpy(fecha_anterior, prec_dia[0].fecha);
    for (int i = 0; i <= struct_count; ++i) {
        if (strcmp(fecha_anterior, prec_dia[i].fecha) == 0) {
            acumulado += prec_dia[i].precipitacion;
        } else {
            printf("<li>%s: %.2f</li>\r\n", fecha_anterior, acumulado);
            /*memset(fecha_anterior, '\0', sizeof(fecha_anterior));*/
            acumulado = prec_dia[i].precipitacion;
            strcpy(fecha_anterior, prec_dia[i].fecha);
        }
    }
    printf("<li>%s: %.2f</li>\r\n", fecha_anterior, acumulado);
    long pos = ftell(fp_buffer);
    fclose(fp_buffer);
    return pos +2;
}

/// Suma todas las muestras de precipitacion de un mes.
/// \param prec_dia  puntero a estructura que contiene el par (fecha, precipitacion) de cada medicion de una estacion.
/// Es el mismo puntero que se le paso a diario_precipitacion.
/// \param struct_count cantidad de elementos en el arreglo prec_dia(valor de return de diario_precipitacion)
/// \return Acumulado mensual de la variable precipitacion
double calcular_acumulado_mensual(struct precipitacion_diaria *prec_dia, int struct_count) {
    double acumulado = 0;
    for (int i = 0; i <= struct_count; ++i) {
        acumulado += prec_dia[i].precipitacion;
    }
    return acumulado;
}

///
/// \param variable string con el nombre de la variable que se desdea obtener el numero de columna en que se ubica en
/// el archivo CSV
/// \return Numero de columna en donde se ubica la variable, -1 en caso de no haber coincidencia con alguno de los
/// valores declarados en el arreglo de constantes 'variables'
int get_columna(char *variable) {
    /*La el arreglo variables[] sirve para contener los nombres de las variables e indicar el numero de columna en la
     * que se encuentra en el archivo CSV(coincide con la posicion en el arreglo)*/
    static const char *variables[
            MAX_COLUMNAS + 1] = {NO_DATA, NO_DATA, NO_DATA, NO_DATA, NO_DATA, "temperatura", "humedad",
                                 "punto_de_rocio", "precipitacion", "velocidad_viento", NO_DATA, "rafaga_maxima",
                                 "presion", "radiacion_solar", "temperatura_suelo_1", "temperatura_suelo_2",
                                 "temperatura_suelo_3", "humedad_suelo_1", "humedad_suelo_2", "humedad_suelo_3",
                                 "humedad_de_hoja"};

    for (int i = 0; i < (MAX_COLUMNAS + 1); ++i) {
        if (strcmp(variable, variables[i]) == 0) {
            return i;
        }
    }
    return -1;
}

/// Guarda todos los valores de una variable en el arreglo w_var[]
/// \param w_var puntero a estructura weather_variable que servira de almacen para el numero de estacion y valor de
/// la variable
/// \param columna numero de columna donde se ubica la variable que se quiere obtener de todas las estaciones(valor de
/// retorno de la funcion get_columna)
/// \return Cantidad de elementos que contiene el arreglo de weather_variable
int get_variables(struct weather_variable *w_var, int columna) {
    FILE *fp_datos_met;
    fp_datos_met = fopen(FILE_DATOS_MET, "r");

    if (fp_datos_met == NULL) {
        perror("Error en get_variables, puntero a archivo nulo ");
        return -1;
    }
    rewind(fp_datos_met);
    int count = 0;
    char buffer_line[LINE_BUFF_SIZE];
    char temp[MID_L];

    while (fgets(buffer_line, LINE_BUFF_SIZE, fp_datos_met) != NULL) {
        w_var[count].numero_estacion = atoi(strtok(buffer_line, SEPARADOR));
        for (int i = 0; i < (columna - 2); ++i) {
            strtok(NULL, SEPARADOR);
        }
        strcpy(temp, strtok(NULL, SEPARADOR));
        if (strcmp(temp, NO_DATA) == 0) {
            w_var[count].variable = 0;
        } else {
            w_var[count].variable = atof(temp);
        }
        count += 1;
        memset(buffer_line, '\0', sizeof(buffer_line));
        memset(temp, '\0', sizeof(temp));
    }
    fclose(fp_datos_met);
    return count;
}

/// Calcula el primedio de una variable para cada estacion.
/// \param buffer_name_promedios nombre de archivo buffer que servirá como almacen de los promedios de la variable de
/// cada estacion
/// \param w_var arreglo de estructura weather_variable que contiene todos los pares (numero_estacion; variable) para
/// realizar el calculo del promedio. Es la misma estructura que se le pasa a la funcion get_variables
/// \param count  Cantidad de elementos que contiene la estructura w_var(valor de retorno de get_variables)
long promedio_variable(char *buffer_name_promedios, struct weather_variable *w_var, int count) {
    FILE *fp;
    fp = fopen(buffer_name_promedios, "w+");
    if (fp == NULL) {
        perror("Error al abrir el buffer en promedio_variable:");
        return -1;
    }
    rewind(fp);
    double acumulado = 0, promedio;
    int cant_valores_estacion = 0;
    int numero_estacion_anerior = w_var[1].numero_estacion;
    printf("\n");

    for (int i = 0; i < count; ++i) {
        if (numero_estacion_anerior == w_var[i].numero_estacion) {
            acumulado += w_var[i].variable;
            cant_valores_estacion += 1;
        } else {
            promedio = acumulado / cant_valores_estacion;
            printf("<li>%d: %.2f</li> \r\n", numero_estacion_anerior, promedio);

            numero_estacion_anerior = w_var[i].numero_estacion;
            acumulado = w_var[i].variable;
            cant_valores_estacion = 1;
        }
    }
    promedio = acumulado / cant_valores_estacion;
    printf("<li>%d: %.2f</li>\r\n", numero_estacion_anerior, promedio);
    long pos = ftell(fp);
    fclose(fp);
    return pos + 2;
}
