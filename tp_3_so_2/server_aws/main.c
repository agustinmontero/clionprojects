#include "main.h"


int main(int argc, char const *argv[]) {
    // printf("argc=%d\n", argc);
    char* var = NULL;
    int numero_estacion = 0, r_check;


    if ((strcmp(argv[1], "descargar") == 0) && (argc == 3)) {
        numero_estacion = atoi(strdup(argv[2]));
        // printf("Hay que descargar la %d\r\n", numero_estacion);
        if (numero_estacion > 0) {
            r_check = exec_descargar_nro_estacion(strdup(argv[2]));
        }
    } else if ((strcmp(argv[1], "promedio") == 0) && (argc == 3)) {
        var = strdup(argv[2]);
        // printf("Calcular promedio de %s\r\n", var);
        r_check = exec_promedio_variable(strdup(argv[2]));
    } else if ((strcmp(argv[1], "diario_precipitacion") == 0) && (argc == 3)) {
        numero_estacion = atoi(argv[2]);
        // printf("Calcular diario_precipitacion de %d\r\n", numero_estacion);
        r_check = exec_diario_precipitacion(strdup(argv[2]) ,0);
    } else if ((strcmp(argv[1], "mensual_precipitacion") == 0) && (argc == 3)) {
        numero_estacion = atoi(argv[2]);
        // printf("Calcular mensual_precipitacion de %d\r\n", numero_estacion);
        r_check = exec_diario_precipitacion(strdup(argv[2]), 1);
    } else if ((strcmp(argv[1], "listar") == 0) && (argc == 3)) {
        // printf("Listar\r\n");
        var = strdup(argv[2]);
        r_check = exec_lisar(var);
    } else if ((strcmp(argv[1], "listar_variables") == 0) && (argc == 3)) {
        // printf("Listar variables\r\n");
        var = strdup(argv[2]);
        r_check = exec_lisar_variables(var);
    }  else {
        // exec_lisar(newsockfd, strdup(buffer_name));
        fprintf(stderr, "Comando invalido!\r\n");
        exit(EXIT_FAILURE);
    }
    if (r_check < 0) {
        // fprintf(stderr, "Error al ejecutar comando\r\n");
    } else {
        // printf("Exito!\r\n");
    }
    return 0;
}



/// Función encargada de ejecutar el comando listar
/// \return -1 si hubo un error, 0 si está bien.
int exec_lisar(char* var) {
    struct information_available estaciones[MAX_ESTACIONES];
    int n, count;
   for (int i = 0; i < MAX_ESTACIONES; ++i) {
       estaciones[i].numero = malloc(sizeof(char)*MID_L);
       memset(estaciones[i].numero, '\0', MID_L);
       strcpy(estaciones[i].numero, NO_DATA);
    }

    count = listar(estaciones);
    if(count == 0){
        fprintf(stderr, "Sin estaciones en la linea %d de %s\r\n", __LINE__, __FILE__);
        return -1;
    }

    // imprimir_estaciones(BUFFER_NAME, estaciones, count);
    printf("<form action=\"%s\" method=\"GET\">\r\n", var);
    printf("<select name=\"estaciones\">\r\n");
    for (size_t i = 0; i < count; i++) {
        printf("<option value=\"%s\" name=\"numero_estacion\">%s</option>\r\n", estaciones[i].numero, estaciones[i].estacion);
    }
    printf("</select>\r\n");
    printf("<br><br>\r\n");
    printf("<input type=\"submit\" value=\"Enviar\">\r\n");
    printf("</form>\r\n");

    n = unlink(strdup(BUFFER_NAME));
    if(n < 0)
        perror("Error al remover archivo ");
    return 0;
}

int exec_lisar_variables(char* var){
    char* nombre_variables[MAX_COLUMNAS] = {NO_DATA, NO_DATA, NO_DATA, NO_DATA,
    "Temperatura [C]","Humedad [HR]","Punto de Rocio [C]","Precipitacion [mm]",
    "Velocidad Viento [Km/h]",NO_DATA,"Rafaga Maxima [Km/h]","Presion [hPa]",
    "Radiacion Solar [W/m2]","Temperatura Suelo 1 [C]","Temperatura Suelo 2 [C]",
    "Temperatura Suelo 3 [C]","Humedad Suelo 1 [grav]","Humedad Suelo 2 [grav]",
    "Humedad Suelo 3 [grav]","Humedad de Hoja []"};
    static const char *variables[MAX_COLUMNAS] = {NO_DATA, NO_DATA, NO_DATA, NO_DATA,
    "temperatura", "humedad", "punto_de_rocio", "precipitacion", "velocidad_viento",
    NO_DATA, "rafaga_maxima", "presion", "radiacion_solar", "temperatura_suelo_1",
    "temperatura_suelo_2", "temperatura_suelo_3", "humedad_suelo_1", "humedad_suelo_2",
    "humedad_suelo_3", "humedad_de_hoja"};

    printf("<form action=\"%s\" method=\"GET\">\r\n", var);
    printf("<select name=\"nombre_variable\">\r\n");
    for (size_t i = 0; i < (MAX_COLUMNAS); i++) {
        if (strcmp(variables[i], NO_DATA) != 0) {
            printf("<option value=\"%s\" name=\"nombre_variable\">%s</option>\r\n", variables[i], nombre_variables[i]);
        }
    }
    printf("</select>\r\n");
    printf("<br><br>\r\n");
    printf("<input type=\"submit\" value=\"Enviar\">\r\n");
    printf("</form>\r\n");
    int n;
    n = unlink(strdup(BUFFER_NAME));
    if(n < 0)
        perror("Error al remover archivo ");
    return 0;
}


/// Ejecuta las instrucciones necesarias para atender el comando 'descargar nro_estacion'
/// \return -1 si hubo algun problema, 0 si esta bien.
int exec_descargar_nro_estacion(char* nro_estacion) {
    // printf("Entradndo a la descarga..\r\n");
    int n;
    FILE *fp_download_file;
    fp_download_file = fopen(DOWNLOAD_FILE_NAME, "w");
    if (fp_download_file == NULL) {
        perror("Error al abrir archivo de descarga");
        return -1;
    }
    n = descargar_nro_estacion(strdup(nro_estacion), fp_download_file);
    if (n <= 0) {
        return -1;
    }
    fclose(fp_download_file);
    // printf("Pass 1..\r\n");
    printf("<a href=\"%s\" download=\"datos_estacion.txt\">Descargar datos de estacion Nro %s</a>\n", DOWNLOAD_FILE_LINK, nro_estacion);
    printf("<br><br>\r\n");
    // printf("<form action=\"%s\" method=\"GET\">\r\n", "/cgi-bin/download_file.cgi");
    // // printf("<select name=\"nombre_archivo\">\r\n");
    // // printf("</select>\r\n");
    // // printf("<br><br>\r\n");
    // printf("<button type=\"submit\" name=\"nombre_archivo\" value=\"%s\">Submit</button><br>\n", DOWNLOAD_FILE_NAME);
    // // printf("<input type=\"submit\" name=\"nombre_archivo\" value=\"%s\">Ir a la descarga</input>\r\n", DOWNLOAD_FILE_NAME);
    // printf("</form>\r\n");
    // printf("Saliendo de la descarga..\r\n");
    return 0;
}

/// Ejecuta las funciones necesarias para atender el comando 'diario_precipitacion' y 'mensual_precipitacion'
/// \param buffer cadena enviada por el cliente
/// \param buffer_name cadena con el nombre del archivo buffer que se va a usar para el cliente, que tiene el formato:
/// 'buffer_nombreDeUsuario.txt'
/// \param usuario puntero a estructura User que contiene los datos principales del cliente que se está atendiendo
/// \param mensual 0 si se quiere atender al comando 'diario_precipitacion, 1 si es para el comando 'mensual_precipitacion'
/// \return 0 si no se detectaron errores, -1 en caso de que algo haya fallado.
int exec_diario_precipitacion(char* nro_estacion, int mensual) {
    int n;
    long buff_length;
    struct precipitacion_diaria precipitaciones[4500];
    // fprintf(stdout, "En 'exec_diario_precipitacion', el numero de estacion que se tiene es %s\n", nro_estacion);

    get_estacion(strdup(BUFFER_NAME_2), nro_estacion);

    int s_count = diario_precipitacion(strdup(BUFFER_NAME_2), precipitaciones);
    if(s_count <= 0){
        fprintf(stderr, "Error en diario_precipitacion: %d\n", s_count);
        return -1;
    }
    if(mensual > 0){ /*Se quiere calcular el acumulado mensul? */
        int m;
        m = exec_mensual_precipitacion(precipitaciones, s_count, strdup(nro_estacion));
        n = unlink(strdup(BUFFER_NAME));
        if(n < 0)
            perror("Error al remover archivo ");
        n = unlink(strdup(BUFFER_NAME_2));
        if(n < 0)
            perror("Error al remover archivo ");
        return m;
    }

    buff_length = calcular_acumulado_diario(strdup(BUFFER_NAME), precipitaciones, s_count);

    if(buff_length < 0){
        fprintf(stderr, "Error al obtener el tamano del archivo en linea %d de %s\r\n", __LINE__, __FILE__);
        return -1;
    }
    n = unlink(strdup(BUFFER_NAME));
    if(n < 0)
        perror("Error al remover archivo ");
    n = unlink(strdup(BUFFER_NAME_2));
    if(n < 0)
        perror("Error al remover archivo ");
    return 0;
}


/// Funcion que termina de ejecutar los comandos de la funcion 'mensual_precipitacion'
/// \param precipitaciones puntero a estructura precipitacion_diaria que contiene todos los pares (fecha, precipitacion)
/// de todas las muestras de una estacion
/// \param s_count cantidad de estructuras almacenadas en el arreglo precipitaciones[]
/// \param nro_estacion cadena con el numero de estacion de la que se desea saber el acumulado mensual de precipitacion.
/// \return -1 si hubo algun problema, 0 si esta bien.
int exec_mensual_precipitacion(struct precipitacion_diaria *precipitaciones, int s_count, char *nro_estacion) {
    double acumulado;
    char cadena[XL_L], temp[SHORT_L];
    acumulado = calcular_acumulado_mensual(precipitaciones, s_count);
    snprintf(temp, SHORT_L - 1, ":\t%.2f[mm]\r\n", acumulado);
    strcpy(cadena, nro_estacion);
    strcat(cadena, temp);
    cadena[XL_L - 1] = '\0';
    printf("<li>%s</li>\r\n", cadena);
    return 0;
}


/// Funcion encargada de ejecutar el comando 'primedio variable'
/// \param buffer cadena enviada por el cliente
/// \param buffer_name cadena con el nombre del archivo buffer que se va a usar para el cliente, que tiene el formato:
/// 'buffer_nombreDeUsuario.txt'
/// \return -1 si hubo algun problema, 0 si esta bien.
int exec_promedio_variable(char *variable) {

    // TODO: hacer el tamaño de la estructura variable con la cantidad de lineas del archivo
    struct weather_variable w_variables[20000];
    int n, columna;
    columna = get_columna(strdup(variable));

    if(columna < 0) {
        fprintf(stderr, "La variable ingresada no es valida!");
        return -1;
    }
    int cantidad = get_variables(w_variables, columna);
    if(cantidad < 0){
        fprintf(stderr, "Error al obtener variable en linea %d de %s\n", __LINE__, __FILE__);
        return -1;
    }
    promedio_variable(strdup(BUFFER_NAME_2), w_variables, cantidad);

    n = unlink(strdup(BUFFER_NAME));
    if(n < 0)
        perror("Error al remover archivo ");
    n = unlink(strdup(BUFFER_NAME_2));
    if(n < 0)
        perror("Error al remover archivo ");
    return 0;
}
