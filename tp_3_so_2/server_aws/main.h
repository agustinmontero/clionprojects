//
// Created by agustin on 24/03/17.
//

#ifndef CVS_PARSER_MAIN_H
#define CVS_PARSER_MAIN_H
#define LINE_BUFF_SIZE 160
#define SEPARADOR ","
#define COMMAND_SEPARATOR " \n"
#define FILE_DATOS_MET "test.txt"
#define USUARIOS "usuarios.txt"
#define DOWNLOAD_FILE_NAME "/var/www/html/files/datos_estacion.txt"
#define DOWNLOAD_FILE_LINK "/files/datos_estacion.txt"
#define CANT_COMANDOS 6
#define FIN "desconectar"
#define NRO_PUERTO 6020
#define MAX_CONEXIONES 5
#define NO_DATA "--"
#define SHORT_L 15
#define MID_L 20
#define LARGE_L 50
#define XL_L 256
#define MAX_ESTACIONES 10
#define MAX_COLUMNAS 20
#define TAM 256
#define BUFFER_NAME "buffer_1.txt"
#define BUFFER_NAME_2 "buffer_2.txt"

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <errno.h>
#include <string.h>


struct information_available {
    char *numero;
    char estacion[LARGE_L];
    char id_localidad[LARGE_L];
    char fecha[LARGE_L];
    char temperatura;
    char humedad;
    char pto_de_rocio;
    char precipitacion;
    char vel_viento;
    char direc_viento;
    char rafaga_min;
    char presion;
    char rad_solar;
    char temp_suelo_1;
    char temp_suelo_2;
    char temp_suelo_3;
    char humedad_suelo_1;
    char humedad_suelo_2;
    char humedad_suelo_3;
    char humedad_hoja;
};

struct precipitacion_diaria {
    char fecha[MID_L];
    double precipitacion;
};

struct weather_variable {
    int numero_estacion;
    double variable;
};

#include "commands.h"
int exec_lisar(char* var);
int exec_lisar_variables(char* var);
int exec_descargar_nro_estacion(char* nro_estacion);
int exec_diario_precipitacion(char* nro_estacion, int mensual);
int exec_mensual_precipitacion(struct precipitacion_diaria *precipitaciones, int s_count, char *nro_estacion);
int exec_promedio_variable(char *variable);
#endif //CVS_PARSER_MAIN_H
