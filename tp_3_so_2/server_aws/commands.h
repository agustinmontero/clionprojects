//
// Created by agustin on 25/03/17.
//

#ifndef CVS_PARSER_COMMANDS_H
#define CVS_PARSER_COMMANDS_H


#include <stdio.h>
#include "main.h"

int listar(struct information_available *estaciones);
long imprimir_estaciones(char *buffer_name, struct information_available *estaciones, int cantidad);
long descargar_nro_estacion(char *numero_estacion, FILE* fp_download_file);
long get_estacion(char *buffer_name, char *numero_estacion);
int diario_precipitacion(char *buffer_temp_name, struct precipitacion_diaria *prec_dia);
long calcular_acumulado_diario(char *buffer_name, struct precipitacion_diaria *prec_dia, int struct_count);
double calcular_acumulado_mensual(struct precipitacion_diaria *prec_dia, int struct_count);
int get_columna(char *variable);
int get_variables(struct weather_variable *w_var, int columna);
long promedio_variable(char *buffer_name_promedios, struct weather_variable *w_var, int count);
#endif //CVS_PARSER_COMMANDS_H
