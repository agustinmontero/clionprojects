#!/usr/bin/perl
# /usr/lib/cgi-bin/upload_file.cgi

use strict;
use warnings;
use CGI;
use CGI::Carp qw(fatalsToBrowser);    # Remove for production use

$CGI::POST_MAX = 1024 * 10;  # maximum upload filesize is 10K

sub save_file($);
sub rmmod_func($);

#
# Build the form
#

my $q = new CGI;
my $untainted_filename;

print $q->header;
print $q->start_html(
    -title => "File upload",
);
print $q->h4("Modulos instalados:"), $q->br;

my $modules = `lsmod`;
my @lines = split /\n/, $modules;
foreach my $line (@lines) {
    print $q->li, $line, $q->br;
}
print $q->h3('Suba el modulo que desea instalar'),
      $q->start_multipart_form(
          -name    => 'main_form');
print 'Seleccopme el archivo: ',
      $q->filefield(
          -name      => 'filename',
	  -size      => 40,
	  -maxlength => 80);
print $q->hr;
print $q->submit(-value => 'Subir archivo');
print $q->hr;
print $q->end_form;

#
# Look for uploads that exceed $CGI::POST_MAX
#

if (!$q->param('filename') && $q->cgi_error()) {
    print $q->cgi_error();
    print <<'EOT';
<p>
Tamaño maximo de archivo excedido.
<p>
contacte al administrador del sistema.
EOT
    print $q->hr, $q->end_html;
    exit 0;
}

#
# Upload the file
#

if ($q->param() && (!$q->param('rmmod_form'))) {
    save_file($q);
} elsif($q->param('rmmod_form')){
    rmmod_func($q);
}



print $q->end_html;
exit 0;

#-------------------------------------------------------------

sub save_file($) {

    my ($q) = @_;
    my ($bytesread, $buffer);
    my $num_bytes = 1024;
    my $totalbytes;
    my $filename = $q->upload('filename');
    my $insmod_result;

    if (!$filename) {
        print $q->p('Elija un archivo!');
	return;
    }

    # Untaint $filename

    if ($filename =~ /^([-\@:\/\\\w]+)\.ko$/) {
        $untainted_filename = $1;
    } else {
        die <<"EOT";
Caracteres no soportados en el nombre de archivo "$filename".
El nombre solo debe contener caracteres alafnumericos,
caracteres'_', '-', '\@', '/' y '\\'.
La extension del archivo debe ser '.ko'.
EOT
    }
    my $file = "/tmp/$filename";

    print "Subiendo $filename a $file<BR>";

    open (OUTFILE, ">", "$file") or die "No se pudo abrir $file para escritura: $!";

    while ($bytesread = read($filename, $buffer, $num_bytes)) {
        $totalbytes += $bytesread;
        print OUTFILE $buffer;
    }
    die "Lectura fallida." unless defined($bytesread);
    unless (defined($totalbytes)) {
        print "<p>Error: No se pudo leer el archivo ${filename}, ";
        print "o tiene longitud nula.";
    } else {
        print "<p>Hecho. Archivo $filename subido a $file ($totalbytes bytes)", $q->br;
    }
    close OUTFILE or die "No se pudo cerrar $file: $!";

    print "Insertando modulo...", $q->br;
    system("sudo -u www-data sudo /sbin/insmod $file");
    if($? != 0){
        die "Error al remover modulo: $!";
    }
    $insmod_result = `dmesg | tail --lines=1`;
    print "Se obtuvo: $insmod_result", $q->br;
    print $q->start_form(-name    => 'rmmod_form');
    print $q->hidden(-name=>'module_name', -default=>$untainted_filename);
    print $q->submit(-value => 'Remover modulo', -name    => 'rmmod_form');
    print $q->end_form;

}
sub rmmod_func($) {
    my $rmmod_result;
    $untainted_filename = $q->param('module_name');
    print $q->h3("Eliminar modulo!"), $q->br;
    print "Removiendo modulo...", $q->br;
    system("sudo -u www-data sudo /sbin/rmmod $untainted_filename");
    if($? != 0){
        die "Error al remover modulo: $!";
    }
    $rmmod_result = `dmesg | tail --lines=1`;
    print "Se obtuvo: $rmmod_result", $q->br;
}

#-------------------------------------------------------------
# sudo insmod hola_mundo.ko && dmesg | tail --lines=1 > mensaje_1.txt && sudo rmmod hola_mundo && dmesg | tail --lines=1 > mensaje_2.txt
#
# sudo ln -s /path/to/module.ko /lib/modules/`uname -r`
# sudo depmod -a
# sudo modprobe module
#
# sudo cp hola_mundo.ko /lib/modules/$(uname -r)/
# sudo depmod -a
# sudo modprobe hola_mundo && dmesg | tail --lines=1
# sudo modprobe -r hola_mundo && dmesg | tail --lines=1
#-------------------------------------------------------------
