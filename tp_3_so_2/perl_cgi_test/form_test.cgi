#!/usr/bin/perl
use CGI;                             # load CGI routines
$q = CGI->new;                        # create new CGI object
print $q->header,                    # create the HTTP header
     $q->start_html('AWS'); # start the HTML

 print $q->start_form(),
 $q->p("Last Name", textfield(-name  => 'lname1',-value => '$lname',)),
 $q->p("First Name", textfield(-name  => 'fname1',-value => '$fname')),
 $q->p("Address", textfield(-name  => 'address1',-value => '$address')),
 $q->p("Zip", textfield(-name  => 'zip1',-value => '$zip')),
 $q->p("Phone", textfield(-name  => 'phone1',-value => '$phone')),
 $q->p("Email", textfield(-name  => 'email1',-value => '$email')),
 $q->p(submit("Update")),
 $q->end_form();

print $q->end_html;
