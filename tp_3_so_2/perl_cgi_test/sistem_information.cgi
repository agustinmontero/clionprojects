#!/usr/bin/perl
use CGI ':standard';

print header( type => 'text/plain',-charset =>'utf-8\n\n' );
my $lscpu = `lscpu`;
my $free = `free -m`;
my $uptime = `uptime`;
my $date = `date`;
print "CPU:\n$lscpu\n";
print "Memoria:\n$free\n";
print "Uptime:\n$uptime\n";
print "Fecha y hora actual:\n$date\n";
