#define MODULE
#define LINUX
#define __KERNEL__

#if defined(CONFIG_MODVERSIONS) && ! defined(MODVERSIONS)
   #include <linux/modversions.h>
   #define MODVERSIONS
#endif

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/fs.h>

int init_module(void);
void cleanup_module(void);
static int device_open(struct inode *, struct file *);
static int device_release(struct inode *, struct file *);
static ssize_t device_read(struct file *, char *, size_t, loff_t *);
static ssize_t device_write(struct file *, const char *, size_t, loff_t *);

#define SUCCESS 0
#define DEVICE_NAME "chardev" /* Dev name as it appears in /proc/devices   */
#define BUF_LEN 80            /* Max length of the message from the device */

static int Major;            /* Major number assigned to our device driver */
static int Device_Open = 0;  /* Is device open?  Used to prevent multiple
                                        access to the device */
static char msg[BUF_LEN];    /* The msg the device will give when asked    */
static char *msg_Ptr;

static struct file_operations fops = {
  .read = device_read,
  .write = device_write,
  .open = device_open,
  .release = device_release
};

int init_module(void)
{
 Major = register_chrdev(0, DEVICE_NAME, &fops);

 if (Major < 0) {
   printk ("Registering the character device failed with %d\n", Major);
   return Major;
 }

 printk("<1>I was assigned major number %d.  To talk to\n", Major);
 printk("<1>the driver, create a dev file with\n");
 printk("'mknod /dev/hello c %d 0'.\n", Major);
 printk("<1>Try various minor numbers.  Try to cat and echo to\n");
 printk("the device file.\n");
 printk("<1>Remove the device file and module when done.\n");

 return 0;
}