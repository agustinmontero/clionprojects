//
// Created by agustin on 19/11/16.
//
#include "malloc_impl.h"
#include <stdio.h>
#include <string.h>

int main(){

    char *cadena1 = (char *)malloc(sizeof(char*));
    char *cadena2 = (char *)malloc(sizeof(char*));

    strcpy(cadena1, "Soy la cadena1");
    strcpy(cadena2, "Soy la cadena2");

    printf("La cadena1 es %s \n", cadena1);
    printf("La cadena2 es %s \n", cadena2);
    free(cadena1);
    free(cadena2);

    return 0;
}
