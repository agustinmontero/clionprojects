#include "malloc_impl.h"

void* base = NULL; 

/* block struct */
struct block_s {
    size_t size;
    struct block_s *next;
    struct block_s *prev;
    int free;
    void *ptr;
    /* A pointer to the allocated block */
    char data [1];
};

#define BLOCK_SIZE sizeof(struct block_s)

void *malloc(size_t size ){
    block_t b,last;
    size_t s;
    s = align4(size );
    if (base) {
        /* First find a block */
        last = base;
        b = find_block (&last ,s);
        if (b) {
            /* can we split */
            if ((b->size - s) >= ( BLOCK_SIZE + 4))
                spliblock_t (b,s);
            b->free =0;
        } else {
            /* No fitting block , extend the heap */
            b = extend_heap (last ,s);
            if (!b)
                return(NULL );
        }
    } else {
        /* first time */
        b = extend_heap (NULL ,s);
        if (!b)
            return(NULL );
        base = b;
    }
    return(b->data );
}

/*Fusion is straightforward: if the next chunk is free, we sum the sizes of the current chunk
and the next one, plus the meta-data size. Then, the we make next field point to the successor
of our successor and if this successor exists we update its predecessor.
*/
block_t fusion(block_t b){
    if (b->next && b->next ->free ){
        b->size += BLOCK_SIZE + b->next ->size;
        b->next = b->next ->next;
        if (b->next)
            b->next ->prev = b;
    }
    return (b);
}

/* Get the block from and addr */
block_t geblock_t (void *p){    
    return p -= BLOCK_SIZE;
}

/* Valid addr for free */
int valid_addr (void *p){
    if (base){
        if ( p>base && p<sbrk (0)){
            return (p == ( geblock_t (p))->ptr );
        }
    }
    return (0);
}

/* The free */
/* See free(3) */
void free(void *p){
    if ( valid_addr (p)){
        block_t b;
        b = geblock_t (p);
        b->free = 1;
        /* fusion with previous if possible */
        if(b->prev && b->prev ->free)
            b = fusion(b->prev );
        /* then fusion with next */
        if (b->next)
            fusion(b);
        else{
            /* free the end of the heap */
            if (b->prev)
                b->prev ->next = NULL;
            else
                /* No more block !*/
                base = NULL;
            brk(b);
        }
    }
}


/* Split block according to size. */
/* The b block must exist. */
void spliblock_t (block_t b, size_t s){
    block_t new;
    new = (block_t )(b->data + s);
    new ->size = b->size - s - BLOCK_SIZE ;
    new ->next = b->next;
    new ->prev = b;
    new ->free = 1;
    new ->ptr = new ->data;
    b->size = s;
    b->next = new;
    if (new ->next)
        new ->next ->prev = new;
}

/* Add a new block at the of heap */
/* return NULL if things go wrong */
block_t extend_heap (block_t last , size_t s){
    int sb;
    block_t b;
    b = sbrk (0);
    sb = (long int) sbrk(BLOCK_SIZE + s);
    if (sb < 0)
        return (NULL );
    b->size = s;
    b->next = NULL;
    b->prev = last;
    b->ptr = b->data;
    if (last)
        last ->next = b;
    b->free = 0;
    return (b);
}

block_t find_block (block_t *last , size_t size ){
    block_t b=base;
    while (b && !(b->free && b->size >= size )) {
        *last = b;
        b = b->next;
    }
    return (b);
}
