//
// Created by agustin on 19/11/16.
//

#ifndef MALLOC_EXAMPLE_MALLOC_EXAMPLE_H
#define MALLOC_EXAMPLE_MALLOC_EXAMPLE_H
#include <stddef.h>
#include <zconf.h>
#define align4(x) (((((x) -1) >>2) <<2)+4)

typedef struct block_s *block_t;
void spliblock_t (block_t b, size_t s);
block_t fusion(block_t b);
block_t find_block (block_t *last , size_t size );
block_t extend_heap (block_t last , size_t s);
void *malloc(size_t size );
void free(void *p);

#endif //MALLOC_EXAMPLE_MALLOC_EXAMPLE_H
