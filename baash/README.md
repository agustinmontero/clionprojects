#Sistemas operativos I
##Trabajo Práctico II - Baash


Se implemento en C una interprete de linea de comandos sencillo para sistemas Linux. La sintaxis que se utiliza es al estilo Bourne Shell:

_**`comando_1 agrumento_1 argumento_2 ... argumento_n`**_

Los operadores que estan implementados son: &, <, > y |. En esta versión, solo se permite usar uno de ellos a la vez.

###Operadores admitidos

**Operador &** : indica a la consola que no espere por la finalizacion de comando_1.

**Operador <** : Instruccion de la forma

`comando_1 arg_1 ...  arg_n < filename`

El comando_1 tomara la entrada desde el archivo _filename_

**Operador >** : Instruccion de la forma

`comando_1 arg_1 ...  arg_n > filename`

La salida que produzca la ejecución del comando_1 será redirigida a un archivo _filename_

**Operador |(pipe)** : El formato de la instruccion es...

`comando_1 arg_1 ...  arg_n | comando_2 arg_2_1 ... arg_2_n`

La salida que produzca la ejecución del comando_1 con sus n argumentos será pasado mediante una tuberia a la entrada
del comando_2.


###Instrucciones

Este release se entrega con un fichero Mekefile. Primero se deberá compilar con el comando **_make_** dentro del directorio .../baash/. 
Para ejecutarlo, desde una terminal: **_./baash_** .
Para remover los archivos compilados y el ejecutable: **_make clean_**.
