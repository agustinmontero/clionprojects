#include "inputs.h"
//
// Created by agustin on 15/10/16.
//
#include <stdlib.h>
#include <memory.h>

/**
 * @return : string con la cadena leida desde stdin
 */
char *read_line(void) {
    char *line = NULL;
    size_t bufsize = 0;
    getline(&line, &bufsize, stdin);
    return line;
}

/**
 *
 * @param line : string leido de la entrada
 * @param argc : int donde se almacenara la cantidad de argumentos del comando1
 * @param argv : arreglo de string donde se almacena el comando1 con sus n argumentos(luego del parseo)
 * @param esp  : int que indica si hay que esperar(1) o no(0) en caso de que se lea el comando &
 * @param pipe_p : int que indica la posicion del operador pipe(|). Si no se encuentra es pipe_p<0
 * @param argc_2 : int donde se almacenara la cantidad de argumentos del comando2
 * @param argv_2 : arreglo de string donde se almacena el comando2(en caso de que haya |) con sus n argumentos
 * @param filename: string para guardar el nombre del file en caso de que se encuentren los argumentos < o >
 * @param r_io : int para indicar si la operacion de redireccion a realizar es de entrada(1) o salida(0). En caso que no
 * sea ninguna de las dos r_io<0
 */
void split_line(char *line, int *argc, char **argv, int *esp, int *pipe_p, int *argc_2, char **argv_2, char *filename,
                int *r_io) {
    int position = 0;
    char *token;

    if (!argv) {
        fprintf(stderr, "allocation error\n");
        exit(EXIT_FAILURE);
    }

    token = strtok(line, TOK_DELIM);
    while (token != NULL) {
        if(strcmp(token, WAIT) == 0){
            *esp = 0;
            break;
        }
        else if(strcmp(token, PIPE) == 0){
            *pipe_p = position;
            argv[position] = NULL;
            position = -1;
        } else if((strcmp(token, R_OUT) == 0) | (strcmp(token, R_IN) == 0)){
            if(strcmp(token, R_OUT) == 0 ) *r_io = 0;
            else *r_io = 1;
            strcpy(filename, strtok(NULL, TOK_DELIM));
            // printf("filename = %s \t, Salida o entrada? %d \n", filename, *r_io);
            break;
        }
        else{
            if(*pipe_p>0){
                argv_2[position] = token;
                *argc_2 += 1;
            } else{
                argv[position] = token;
                *argc += 1;
            }
        }
        position++;
        token = strtok(NULL, TOK_DELIM);
    }

    if(*pipe_p>0){
        argv_2[position] = NULL;
    } else{
        argv[position] = NULL;
    }
}


/**
 *
 * @param commands : arreglo de strings donde se guardaran todos los path posibles para la ejecucion del programa
 * @param argv0 : comando que sera concatenado al final de cada ruta posible.
 */
void get_command_list(char **commands, char *argv0) {
    char * const path_list = getenv("PATH");
    char * tok = strtok(path_list, ":");
    size_t buffer = 256;
    char **path = (char **) malloc(buffer * sizeof(commands));
    int p = 0;
    while (tok != NULL){
        path[p] = tok;
        tok = strtok(NULL, ":");
        p += 1;
    }
    path[p] = NULL;
    p=0;
    while(path[p] != NULL){
        commands[p+1] = strdup(path[p]);
        strcat(commands[p+1], "/");
        strcat(commands[p+1], argv0);
        p += 1;
    }
    commands[p+1] = NULL;
}
