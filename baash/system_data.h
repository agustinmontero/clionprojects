//
// Created by agustin on 15/10/16.
//

#ifndef BAASH_SYSTEM_DATA_H
#define BAASH_SYSTEM_DATA_H
char* get_hostname(void);
const char* get_user_name(void);
char* get_directory_path(void);
char* get_baash_prompt(void);

#endif //BAASH_SYSTEM_DATA_H
