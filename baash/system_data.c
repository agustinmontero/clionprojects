//
// Created by agustin on 15/10/16.
//
#include <pwd.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include "system_data.h"

/**
 * @return : string con el hostname
 */
char *get_hostname(void) {
    FILE *fp;
    size_t nbytes = 50;
    char* hostname_str;

    fp = fopen ("/proc/sys/kernel/hostname", "r");
    hostname_str = (char *) malloc(nbytes+ 1);
    ssize_t chars = getline(&hostname_str, &nbytes, fp);
    if ((hostname_str)[chars - 1] == '\n')
        (hostname_str)[chars - 1] = '\0';
    fclose(fp);
    return hostname_str;
}

/**
 * @return : string con el nombre del usuario actual.
 */
const char *get_user_name(void) {
    uid_t uid = geteuid();
    struct passwd *pw = getpwuid(uid);
    if (pw)
    {
        return pw->pw_name;
    }

    return "";
}

/**
 * @return : string con el directorio actual de trabajo
 */
char *get_directory_path(void) {
    char cwd[1024];
    char *str = cwd;
    if (getcwd(cwd, sizeof(cwd)) != NULL){
        strcpy(str, cwd);
        return str;
    }
        //fprintf(stdout, "Current working dir: %s\n", cwd);

    else
        perror("getcwd() error");
    return NULL;
}

/**
 * @return : string con el promt formateado, listo para imprimir en pantalla
 */
char *get_baash_prompt(void) {
    char *prompt = (char *) get_user_name();
    strcat(prompt, "@");
    strcat(prompt, get_hostname());
    strcat(prompt, " ");
    strcat(prompt, get_directory_path());
    strcat(prompt, " $");
    return prompt;
}
