#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <fcntl.h>
#include "system_data.h"
#include "inputs.h"

#define N 256
#define EXIT_APP "exit"
#define CHANGE_DIR "cd"
#define P_READ 0 // leer pipe
#define P_WRITE 1 // escribir pipe

int main(int argc, char **argv) {

    do {
        int child_to_p_pipe[2];
        if(pipe(child_to_p_pipe)<0){perror("child_to_p_pipe"); _exit(1);}

        char *prompt = get_baash_prompt();
        printf("%s", prompt);

        int esperar = 1; // Esperar al hijo? 1 → Si | 0 → No
        int r_io = -1;// 0 → Out | 1 → In | -1 → None
        int pipe_command = -1; // Posiscion del caracter |
        char **commands = (char **) malloc(N * sizeof(char*));
        char **my_args = (char **) malloc(N * sizeof(char*));
        int arg_count = 0;
        char **my_args_2 = (char **) malloc(N * sizeof(char*));
        char **commands_2 = (char **) malloc(N * sizeof(char*));
        int arg_count_2 = 0;
        char *filename = (char *) malloc(sizeof(char*));
        char* line = read_line();

        split_line(strdup(line), &arg_count, my_args, &esperar, &pipe_command, &arg_count_2, my_args_2, filename,
                   &r_io);

        if (my_args[0] == NULL) continue; // comando vacio
        if (strcmp(my_args[0], EXIT_APP) == 0) return 0; //comando exit
        if (strcmp(CHANGE_DIR, my_args[0]) == 0) {// comando cd
            if (my_args[1] == NULL) {
                fprintf(stderr, "Expected argument to \"cd\"\n");
            } else {
                if (chdir(my_args[1]) != 0) {
                    perror("No se pudo mover al directorio\n");
                }
            } continue;
        }

        int fork_id = fork();
        int status;
        if (fork_id == 0) {// Entran solo hijos
            int fid = 0;
            if(r_io >= 0){// Hay comando de redireccion < o >
                if(r_io == 0){// poner salida en filename
                    fid = open(filename, O_RDWR | O_CREAT, S_IRUSR | S_IWUSR);
                    if (fid<0) { perror("open");exit(1); }
                    if (dup2(fid, STDOUT_FILENO)<0) { perror("dup2"); exit(1); }
                    close(fid);
                } else{// tomar entrada de filename
                    fid = open(filename, O_RDONLY, 0);
                    if (fid<0) { perror("open");exit(1); }
                    if (dup2(fid, STDIN_FILENO)<0) { perror("dup2"); exit(1); }
                    close(fid);
                }
                /*Now the child has stdin coming from the input file,stdout going to the output file, and no extra
                 * files open, it is safe to execute the command to be executed.*/
            }
            if(pipe_command > 0){ // Hay comando de |
                close (child_to_p_pipe[P_READ]);
                dup2 (child_to_p_pipe[P_WRITE], STDOUT_FILENO);
                close (child_to_p_pipe[P_WRITE]);
            }


            commands[0] = strdup(my_args[0]);
            get_command_list(commands, strdup(my_args[0]));
            int j = 0;
            while (access(commands[j], X_OK & (j<N))){
                j += 1;
            }
            int st= execv(commands[j], my_args);
            return st;
        } else{ // soy padre
            if(esperar == 1){
                if(pipe_command > 0){//hay comando | para ejecutar comando2
                    int fork_id_2 = fork();
                    int status_2;
                    if(fork_id_2<0) perror("fork() fail \n");
                    else if(fork_id_2 == 0){// hijo que recibe de pipe  y ejecuta comando2...
                        close (child_to_p_pipe[P_WRITE]);
                        if(dup2 (child_to_p_pipe[P_READ], STDIN_FILENO)<0){perror("error e dup2 hijo chico\n"); exit(1);}
                        close (child_to_p_pipe[P_READ]);

                        commands_2[0] = strdup(my_args_2[0]);
                        get_command_list(commands_2, strdup(my_args_2[0]));
                        int j = 0;
                        while (access(commands_2[j], X_OK)){
                            j += 1;
                        }
                        int st= execv(commands_2[j], my_args_2);
                        return st;
                    } else
                        wait(&status_2);
                } else
                    wait(&status);
            }
            printf("\n");
        }
    } while (1);
}
