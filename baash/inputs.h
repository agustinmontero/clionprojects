//
// Created by agustin on 26/10/16.
//

#ifndef BAASH_INPUTS_C_H
#define BAASH_INPUTS_C_H
#include <stdio.h>
#define TOK_DELIM " \t\r\n\a"
#define WAIT "&"
#define PIPE "|"
#define R_OUT ">"
#define R_IN "<"

char *read_line(void);
void split_line(char *line, int *argc, char **argv, int *esp, int *pipe_p, int *argc_2, char **argv_2, char *filename,
                int *r_io);

void get_command_list(char **commands, char *argv0);

#endif //BAASH_INPUTS_C_H
