//
// Created by agustin on 29/03/17.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "parser.h"


/// Parseo de la cadena inicial para loguear al usuario
/// \param usuario Estructura User para contener los datos del usuario
/// \param cadena Cadena con el nombre de usuario y passwrod(se espera algo de la forma 'usuario,password')
/// \return 0 si el parse fue correcto, -1 si hubo algun problema
int parse_user(User *usuario, char *cadena) {
    if(cadena == NULL){
        fprintf(stderr, "Error en %s, cadena NULL\n", __FILE__);
        return -1;
    }
    strcpy(usuario->nombre, strtok(cadena, SEPARADOR));
    if(usuario->nombre == NULL){
        fprintf(stderr, "Error en %s, nombre de usuario NULL\n", __FILE__);
        return -1;
    }
    strcpy(usuario->password, strtok(NULL, SEPARADOR));
    if(usuario->password == NULL){
        fprintf(stderr, "Error en %s, password NULL\n", __FILE__);
        return -1;
    }
    char *temp = strtok(NULL, SEPARADOR);
    if(temp != NULL){
        fprintf(stderr, "Exceso de argumentos en linea %d de %s\n", __LINE__, __FILE__);
        return -1;
    }
    return 0;
}

/// Funcion para parsear el comando enviado por el usuario y extraer del mismo el numero de estacion.
/// \param cadena Cadena enviada por el usuario, se espera algo de la forma: 'comando nro_de_estacion'.
/// \param numero_estacion Cadena donde se va a almacenar el numero de estacion en caso de que se encuentre.
/// \return 0 si hubo exito, -1 en caso de error.
/// \bug No se comprueba que la cadena almacenada en numero_estacion sea efectivamente un numero.
int get_numero_estacion(char *cadena, char *numero_estacion) {
    char *temp = strtok(cadena, COMMAND_SEPARATOR);
    if(temp == NULL){
        fprintf(stderr, "Sin numero de estacion en %s linea %d\n", __FILE__, __LINE__);
        return -1;
    }
    strcpy(numero_estacion, strtok(NULL, COMMAND_SEPARATOR));
    if(numero_estacion == NULL){
        fprintf(stderr, "Sin numero de estacion en %s linea %d\n", __FILE__, __LINE__);
        return -1;
    } else{
        temp = strtok(NULL, COMMAND_SEPARATOR);
        if(temp != NULL){
            fprintf(stderr, "Sin numero de estacion -> Exceso de argumentos\n");
            return -1;
        }
    }
    numero_estacion[MID_L - 1] = '\0';
    return 0;
}

///
/// \param comm comando enviado por el usuario
/// \return Devuelve la posicion en el arreglo commandos[] en que se encuentra el comando principal enviado por el
/// usuario, -1 si no hubo coincidencia.
int get_numero_comando(char *comm) {
    const char *comandos[CANT_COMANDOS] = {"listar", "descargar", "diario_precipitacion", "mensual_precipitacion",
                                                  "promedio", "desconectar"};
    if(comm == NULL){
        return -1;
    }
    char *comando = strtok(comm, COMMAND_SEPARATOR);
    int numero_comando = -1;
    for (int i = 0; i < CANT_COMANDOS; ++i) {
        if(strcmp(comando, comandos[i]) == 0){
            numero_comando = i;
        }
    }
    return numero_comando;
}

/// Funcion para obtener el nombre de la variable en comando 'promedio variable'
/// \param cadena Comando enviado por el usuario
/// \param variable String donde se almacenará el nombre de la variable
/// \return -1 si no se encontró ningun string para almacenar en variable o si hay mas parametros de los esperados en
/// cadena. 0 si está bien.
int get_variable(char *cadena, char *variable) {
    char *temp = strtok(cadena, COMMAND_SEPARATOR);
    if(temp == NULL){
        fprintf(stderr, "Sin numero de estacion en %s linea %d\n", __FILE__, __LINE__);
        return -1;
    }
    strcpy(variable, strtok(NULL, COMMAND_SEPARATOR));
    if(variable == NULL){
        fprintf(stderr, "Sin variable en %s linea %d\n", __FILE__, __LINE__);
        return -1;
    } else{
        temp = strtok(NULL, COMMAND_SEPARATOR);
        if(temp != NULL){
            fprintf(stderr, "Exceso de argumentos en linea %d de %s\n", __LINE__, __FILE__);
            return -1;
        }
    }
    variable[MID_L - 1] = '\0';
    return 0;
}
