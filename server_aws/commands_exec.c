//
// Created by agustin on 31/03/17.
//

#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include "main.h"
#include "commands_exec.h"
#include "commands.h"
#include "server_libs.h"
#include "parser.h"

/// Función encargada de ejecutar el comando listar
/// \param newsockfd Descriptor del socket para la comunicacion TCP/IP con el cliente
/// \param buffer_name cadena con el nombre del archivo buffer que se va a usar para el cliente, que tiene el formato:
/// 'buffer_nombreDeUsuario.txt'
/// \return -1 si hubo un error, 0 si está bien.
int exec_lisar(int newsockfd, char *buffer_name) {
    struct information_available estaciones[MAX_ESTACIONES];
    long buff_length;
    int n, count;
   for (int i = 0; i < MAX_ESTACIONES; ++i) {
       estaciones[i].numero = malloc(sizeof(char)*MID_L);
       memset(estaciones[i].numero, '\0', MID_L);
       strcpy(estaciones[i].numero, NO_SRV_DATA);
    }

    count = listar(estaciones);
    if(count == 0){
        fprintf(stderr, "Sin estaciones en la linea %d de %s\n", __LINE__, __FILE__);
        return -1;
    }

    buff_length = imprimir_estaciones(strdup(buffer_name), estaciones, count);
    if(buff_length <= 0){
        fprintf(stderr, "Error linea %d de %s\n", __LINE__, __FILE__);
        return -1;
    }

    char cadena[buff_length];
    n = get_cadena(strdup(buffer_name), cadena, (size_t) buff_length);
    if(n <= 0){
        fprintf(stderr, "Error al obtener la cadena en linea %d de %s\n", __LINE__, __FILE__);
        return -1;
    }
    cadena[buff_length - 1] = '\0';

    enviar_tamano_buffer(newsockfd, buff_length);

    n = (int) write(newsockfd, cadena, (sizeof(char)*buff_length));
    if (n < 0) {
        perror("escritura en socket");
        fprintf(stderr, "En la linea %d de %s -> Numero de descriptor %d\n", __LINE__, __FILE__, newsockfd);
        exit(EXIT_FAILURE);
    }
    n = unlink(strdup(buffer_name));
    if(n < 0)
        perror("Error al remover archivo ");
    return 0;
}


/// Ejecuta las instrucciones necesarias para atender el comando 'descargar nro_estacion'
/// \param newsockfd Descriptor del socket para la comunicacion TCP/IP con el cliente
/// \param buffer cadena enviada por el cliente
/// \param buffer_name cadena con el nombre del archivo buffer que se va a usar para el cliente, que tiene el formato:
/// 'buffer_nombreDeUsuario.txt'
/// \return -1 si hubo algun problema, 0 si esta bien.
int exec_descargar_nro_estacion(int newsockfd, char *buffer, char *buffer_name) {
    char nro_estacion[MID_L];
    int n, n_est_control;
    long buff_length;

    n_est_control = get_numero_estacion(strdup(buffer), nro_estacion);
    if(n_est_control < 0){
        fprintf(stderr, "No se puede obtener el numero de estacion\n");
        return -1;
    }
    buff_length = descargar_nro_estacion(strdup(buffer_name), strdup(nro_estacion));

    fprintf(stdout, "El tamaño del buffer que voy a reservar es %li, estoy en la linea %d\n", buff_length, __LINE__);

    if(buff_length < 0){
        fprintf(stderr, "Error tamaño de buffer en linea %d de %s\n", __LINE__, __FILE__);
        return -1;
    }
    char cadena[buff_length];
    memset(cadena, '\0', sizeof(cadena));

    n = get_cadena(strdup(buffer_name), cadena, (size_t) buff_length);
    if(n <= 0){
        return -1;
    }
    cadena[buff_length - 1] = '\0';

    enviar_tamano_buffer(newsockfd, buff_length);

    n = enviar_archivo(cadena, buff_length);
    if (n < 0) {
        fprintf(stderr, "En la linea %d de %s -> Error al enviar archivo \n", __LINE__, __FILE__);
        return -1;
    }
    n = unlink(strdup(buffer_name));
    if(n < 0)
        perror("Error al remover archivo ");
    return 0;
}

/// Ejecuta las funciones necesarias para atender el comando 'diario_precipitacion' y 'mensual_precipitacion'
/// \param newsockfd Descriptor del socket para la comunicacion TCP/IP con el cliente
/// \param buffer cadena enviada por el cliente
/// \param buffer_name cadena con el nombre del archivo buffer que se va a usar para el cliente, que tiene el formato:
/// 'buffer_nombreDeUsuario.txt'
/// \param usuario puntero a estructura User que contiene los datos principales del cliente que se está atendiendo
/// \param mensual 0 si se quiere atender al comando 'diario_precipitacion, 1 si es para el comando 'mensual_precipitacion'
/// \return 0 si no se detectaron errores, -1 en caso de que algo haya fallado.
int exec_diario_precipitacion(int newsockfd, char *buffer, char *buffer_name, User *usuario, int mensual) {
    char nro_estacion[MID_L];
    int n, n_est_control;
    long buff_length;

    struct precipitacion_diaria precipitaciones[4500];
    n_est_control = get_numero_estacion(strdup(buffer), nro_estacion);
    if(n_est_control < 0){
        fprintf(stderr, "No se puede obtener el numero de estacion\n");
        return -1;
    }
    nro_estacion[MID_L-1] = '\0';
    fprintf(stdout, "En 'exec_diario_precipitacion', el numero de estacion que se tiene es %s\n", nro_estacion);

    char buffer_temp_name[XL_L];
    memset(buffer_temp_name, '\0', XL_L);

    strcpy(buffer_temp_name, usuario->nombre);
    strcat(buffer_temp_name, "_buffer_temp");

    descargar_nro_estacion(strdup(buffer_temp_name), nro_estacion);

    int s_count = diario_precipitacion(strdup(buffer_temp_name), precipitaciones);
    if(s_count <= 0){
        fprintf(stderr, "Error en diario_precipitacion: %d\n", s_count);
        return -1;
    }
    if(mensual > 0){ /*Se quiere calcular el acumulado mensul? */
        int m;
        m = exec_mensual_precipitacion(newsockfd, precipitaciones, s_count, strdup(nro_estacion));
        n = unlink(strdup(buffer_name));
        if(n < 0)
            perror("Error al remover archivo ");
        n = unlink(strdup(buffer_temp_name));
        if(n < 0)
            perror("Error al remover archivo ");
        return m;
    }

    buff_length = calcular_acumulado_diario(strdup(buffer_name), precipitaciones, s_count);

    if(buff_length < 0){
        fprintf(stderr, "Error al obtener el tamano del archivo en linea %d de %s\n", __LINE__, __FILE__);
        return -1;
    }

    char cadena[buff_length];
    n = get_cadena(strdup(buffer_name), cadena, (size_t) buff_length);
    if(n <= 0){
        fprintf(stderr, "Error al obtener la cadena en linea %d de %s\n", __LINE__, __FILE__);
        return -1;
    }
    cadena[buff_length - 1] = '\0';

    enviar_tamano_buffer(newsockfd, buff_length);

    n = (int) write(newsockfd, cadena, (sizeof(char)*buff_length));
    if (n < 0) {
        perror("escritura en socket");
        fprintf(stderr, "En la linea %d de %s -> Numero de descriptor %d\n", __LINE__, __FILE__, newsockfd);
        exit(EXIT_FAILURE);
    }

    n = unlink(strdup(buffer_name));
    if(n < 0)
        perror("Error al remover archivo ");
    n = unlink(strdup(buffer_temp_name));
    if(n < 0)
        perror("Error al remover archivo ");
    return 0;
}


/// Funcion que termina de ejecutar los comandos de la funcion 'mensual_precipitacion'
/// \param newsockfd Descriptor del socket para la comunicacion TCP/IP con el cliente
/// \param precipitaciones puntero a estructura precipitacion_diaria que contiene todos los pares (fecha, precipitacion)
/// de todas las muestras de una estacion
/// \param s_count cantidad de estructuras almacenadas en el arreglo precipitaciones[]
/// \param nro_estacion cadena con el numero de estacion de la que se desea saber el acumulado mensual de precipitacion.
/// \return -1 si hubo algun problema, 0 si esta bien.
int exec_mensual_precipitacion(int newsockfd, struct precipitacion_diaria *precipitaciones, int s_count,
                               char *nro_estacion) {
    double acumulado;
    int n;
    char cadena[XL_L], temp[SHORT_L];
    acumulado = calcular_acumulado_mensual(precipitaciones, s_count);
    snprintf(temp, SHORT_L - 1, ":\t%.2f[mm]\n", acumulado);
    strcpy(cadena, nro_estacion);
    strcat(cadena, temp);
    cadena[XL_L - 1] = '\0';
    long buff_length;
    buff_length = (strlen(cadena) + 1);

    enviar_tamano_buffer(newsockfd, buff_length);

    n = (int) write(newsockfd, cadena, (sizeof(char)*buff_length));
    if (n < 0) {
        perror("escritura en socket");
        fprintf(stderr, "En la linea %d de %s -> Numero de descriptor %d\n", __LINE__, __FILE__, newsockfd);
        exit(EXIT_FAILURE);
    }
    return 0;
}


/// Funcion encargada de ejecutar el comando 'primedio variable'
/// \param newsockfd Descriptor del socket para la comunicacion TCP/IP con el cliente
/// \param buffer cadena enviada por el cliente
/// \param buffer_name cadena con el nombre del archivo buffer que se va a usar para el cliente, que tiene el formato:
/// 'buffer_nombreDeUsuario.txt'
/// \return -1 si hubo algun problema, 0 si esta bien.
int exec_promedio_variable(int newsockfd, char *buffer, char *buffer_name) {

    // TODO: hacer el tamaño de la estructura variable con la cantidad de lineas del archivo
    struct weather_variable w_variables[19000];
    char buffer_name_promedios[LARGE_L], variable[MID_L];
    int v_control, n;
    long buff_length;
    strncpy(buffer_name_promedios, buffer_name, 30);
    strcat(buffer_name_promedios, "_promedios"); // formato: buffer_nombreUsuario_promedios

    v_control = get_variable(strdup(buffer), variable);
    if(v_control < 0){
        fprintf(stderr, "Error al obtener variable\n");
        return -1;
    }

    int columna = get_columna(strdup(variable));

    if(columna < 0) {
        fprintf(stderr, "La variable ingresada no es valida!");
        return -1;
    }
    int cantidad = get_variables(w_variables, columna);
    if(cantidad < 0){
        fprintf(stderr, "Error al obtener variable en linea %d de %s\n", __LINE__, __FILE__);
        return -1;
    }
    buff_length = promedio_variable(strdup(buffer_name_promedios), w_variables, cantidad);

    if(buff_length <= 0){
        fprintf(stderr, "Error al obtener el tamano del archivo en linea %d de %s\n", __LINE__, __FILE__);
        return -1;
    }
    char cadena[buff_length];
    memset(cadena, '\0', sizeof(cadena));

    n = get_cadena(strdup(buffer_name_promedios), cadena, (size_t) buff_length);
    if(n <= 0){
        fprintf(stderr, "Error al obtener la cadena en linea %d de %s\n", __LINE__, __FILE__);
        return -1;
    }

    enviar_tamano_buffer(newsockfd, buff_length);

    n = (int) write(newsockfd, cadena, (sizeof(char)*buff_length));
    if (n < 0) {
        perror("escritura en socket");
        fprintf(stderr, "En la linea %d de %s -> Numero de descriptor %d\n", __LINE__, __FILE__, newsockfd);
        exit(EXIT_FAILURE);
    }
    n = unlink(strdup(buffer_name));
    if(n < 0)
        perror("Error al remover archivo ");
    n = unlink(strdup(buffer_name_promedios));
    if(n < 0)
        perror("Error al remover archivo ");
    return 0;
}


/// Funcion encargada de enviarle al cliente en un mensaje la cantidad de bytes que tendra el proximo mensaje.
/// \param newsockfd Descriptor del socket para la comunicacion TCP/IP con el cliente
/// \param len cantidad de bytes que tendra el proximo mensaje
/// \return 0 si esta bien.
int enviar_tamano_buffer(int newsockfd, long len) {
    int n;
    len = (sizeof(char) * len);
    char cadena[XL_L];
    memset(cadena, '\0', XL_L);
    sprintf(cadena, "%ld", len);
    /*cadena[XL_L - 1] = '\0';*/
    fprintf(stdout, "Tamaño de buffer %s, en linea %d de %s\n", cadena, __LINE__, __FILE__);
    n = (int) write(newsockfd, cadena, sizeof(cadena));
    if (n < 0) {
        perror("escritura en socket");
        fprintf(stderr, "En la linea %d de %s -> Numero de descriptor %d\n", __LINE__, __FILE__, newsockfd);
        exit(EXIT_FAILURE);
    }
    return 0;
}

int enviar_archivo(char* cadena, long buff_length){
    int sockfd, tamano_direccion;
	char buffer[ TAM ];
	struct sockaddr_in serv_addr;
	int n;

	sockfd = socket( AF_INET, SOCK_DGRAM, 0 );
	if (sockfd < 0) {
		perror("ERROR en apertura de socket");
		return -1;
	}

	memset( &serv_addr, 0, sizeof(serv_addr) );
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons( UDP_PORT );
	memset( &(serv_addr.sin_zero), '\0', 8 );

	if( bind( sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr) ) < 0 ) {
		perror( "ERROR en binding" );
		return -1;
	}

    printf( "UDP Socket disponible: %d\n", ntohs(serv_addr.sin_port) );

	tamano_direccion = sizeof( struct sockaddr );

	memset( buffer, 0, TAM );
	n = recvfrom( sockfd, buffer, TAM-1, 0, (struct sockaddr *)&serv_addr, (socklen_t *)&tamano_direccion );
	if ( n < 0 ) {
		perror( "lectura de socket" );
        close(sockfd);
		return -1;
	}
	printf( "Recibí: %s\nEnviando archivo...\n", buffer );


    int bytes_restantes = buff_length;
    int a = 0, chunck, count = 0;

    while (bytes_restantes > 0) {
        count += 1;
        chunck = bytes_restantes - MAX_UDP_BUF_SIZE;
        bytes_restantes -= MAX_UDP_BUF_SIZE;
        char* buffer_temp;
        if (chunck >= 0) {
            buffer_temp = strndup(cadena + a, (size_t) MAX_UDP_BUF_SIZE);
        } else {
            buffer_temp = strndup(cadena + a, (size_t) buff_length - 1);
        }
        if (buffer_temp == NULL) {
            perror("Error en buffer_temp: ");
            close(sockfd);
            return -1;
        }
        // printf("Tamaño de buffer_temp=%d\n", (int)strlen(buffer_temp));
        a += MAX_UDP_BUF_SIZE;
        n = sendto(sockfd, buffer_temp, MAX_UDP_BUF_SIZE + 1, 0, (struct sockaddr *)&serv_addr, (socklen_t) tamano_direccion);
        if ( n < 0 ) {
            perror( "Error escritura en socket" );
            close(sockfd);
            return -1;
        }
    }
    printf("sendto -> count=%d\n", count);
    close(sockfd);
	return 0;
}
