//
// Created by agustin on 29/03/17.
//

#include "main.h"

#ifndef CVS_PARSER_PARSER_H
#define CVS_PARSER_PARSER_H
int get_numero_comando(char *comm);
int parse_user(User *usuario, char *cadena);
int get_numero_estacion(char *cadena, char *numero_estacion);
int get_variable(char *cadena, char *variable);
#endif //CVS_PARSER_PARSER_H
