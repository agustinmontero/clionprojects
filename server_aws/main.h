//
// Created by agustin on 24/03/17.
//

#ifndef CVS_PARSER_MAIN_H
#define CVS_PARSER_MAIN_H
#define LINE_BUFF_SIZE 160
#define SEPARADOR ","
#define COMMAND_SEPARATOR " \n"
#define FILE_DATOS_MET "test.txt"
#define USUARIOS "usuarios.txt"
#define CANT_COMANDOS 6
#define FIN "desconectar"
#define NRO_PUERTO 6020
#define MAX_CONEXIONES 5
#define NO_SRV_DATA "--"
#define SHORT_L 15
#define MID_L 20
#define LARGE_L 50
#define XL_L 256
#define MAX_ESTACIONES 10
#define MAX_COLUMNAS 20
#define TAM 256

struct information_available {
    char *numero;
    char estacion[LARGE_L];
    char id_localidad[LARGE_L];
    char fecha[LARGE_L];
    char temperatura;
    char humedad;
    char pto_de_rocio;
    char precipitacion;
    char vel_viento;
    char direc_viento;
    char rafaga_min;
    char presion;
    char rad_solar;
    char temp_suelo_1;
    char temp_suelo_2;
    char temp_suelo_3;
    char humedad_suelo_1;
    char humedad_suelo_2;
    char humedad_suelo_3;
    char humedad_hoja;
};

struct precipitacion_diaria {
    char fecha[MID_L];
    double precipitacion;
};

struct weather_variable {
    int numero_estacion;
    double variable;
};

typedef struct usuario{
    char nombre[MID_L];
    char password[MID_L];
} User;
#endif //CVS_PARSER_MAIN_H
