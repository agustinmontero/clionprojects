//
// Created by agustin on 30/03/17.
//

#ifndef CVS_PARSER_SERVER_LIBS_H
#define CVS_PARSER_SERVER_LIBS_H
#include "main.h"

int validar_usuario(User *usuario);
int get_cadena(char *buffer_name, char *cadena, size_t length);

#endif //CVS_PARSER_SERVER_LIBS_H
