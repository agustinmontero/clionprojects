//
// Created by agustin on 30/03/17.
//
#include <stdio.h>
#include <string.h>
#include "server_libs.h"

/// Comprueba que el usuario este registrado en el sistema.
/// \param usuario estructura que contiene el nombre de usuario y passwrod para compararlos con los almacenados en
/// el servidro
/// \return 0 si el usuario y password coinciden con alguno de los existentes, -1 en caso contrario.
int validar_usuario(User *usuario) {
    FILE *fp = fopen(USUARIOS, "r");
    if(fp == NULL){
        perror("Error al abriri el archivo de usuarios: ");
        return -1;
    }
    rewind(fp);
    char buffer_line[XL_L], temp_user[MID_L], temp_pass[MID_L];
    while(fgets(buffer_line, XL_L - 1, fp) != NULL){
        strcpy(temp_user, strtok(buffer_line, SEPARADOR));
        if(strcmp(usuario->nombre, temp_user) == 0){
            strcpy(temp_pass, strtok(NULL, "\n"));
            if(strcmp(usuario->password, temp_pass) == 0){
                fclose(fp);
                return 0;
            }
        }
        memset(buffer_line, 0, sizeof(buffer_line));
        memset(temp_pass, 0, sizeof(temp_pass));
        memset(temp_user, 0, sizeof(temp_user));
    }
    fclose(fp);
    return -1;
}

/// Obtiene un string a partir de un archivo.
/// \param buffer_name string con el nombre del archivo buffer. Contiene el texto que se desea guardar en la variable
/// cadena
/// \param cadena string para almacenar el contenido del archivo
/// \return Cantidad de lienas que se leyeron del archivo fp_buffdes
int get_cadena(char *buffer_name, char *cadena, size_t length) {
    FILE *fp_buff;
    fp_buff = fopen(buffer_name, "r");
    if(fp_buff == NULL){
        perror("Error abriendo archivo: ");
        return -1;
    }
    rewind(fp_buff);
    int n;
    n = (int) fread(cadena, sizeof(char), length, fp_buff);
    fclose(fp_buff);
    return n;
}
