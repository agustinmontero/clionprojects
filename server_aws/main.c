#include "main.h"
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <errno.h>
#include "parser.h"
#include "server_libs.h"
#include "commands_exec.h"

int main() {

    int sockfd, clilen, n;
    char buffer[TAM];
    struct sockaddr_in serv_addr, cli_addr;

    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) {
        perror(" apertura de socket ");
        exit(EXIT_FAILURE);
    }

    memset((char *) &serv_addr, 0, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    serv_addr.sin_port = htons((uint16_t) NRO_PUERTO);

    if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
        perror("Error en bind() ");
        exit(EXIT_FAILURE);
    }

    printf("Proceso: %d - socket disponible: %d\n", getpid(), ntohs(serv_addr.sin_port));

    listen(sockfd, MAX_CONEXIONES);
    clilen = sizeof(cli_addr);

    while (1) {
        int newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, (socklen_t *) &clilen);
        if (newsockfd < 0) {
            perror("accept");
            exit(EXIT_FAILURE);
        }

        int pid = fork();
        if (pid < 0) {
            perror("fork");
            exit(EXIT_FAILURE);
        }

        if (pid == 0) {  // Proceso hijo
            close(sockfd);
            memset(buffer, 0, TAM);
            n = (int) read(newsockfd, buffer, TAM - 1);
            printf("Recibi: \n%s\n", buffer);
            if (n < 0) {
                perror("lectura de socket");
                exit(EXIT_FAILURE);
            }
            int valid_user, r_check = 0;
            User *usuario = malloc(sizeof(User));
            valid_user = parse_user(usuario, buffer);
            if(valid_user == 0){
                valid_user = validar_usuario(usuario);
            }
            if(valid_user < 0){
                fprintf(stderr, "Usuario o contraseña invalida\n");
                n = (int) write(newsockfd, "-1", 3);
                if (n < 0) {
                    perror("escritura en socket");
                    exit(EXIT_FAILURE);
                }
                exit(EXIT_SUCCESS);
            }
            n = (int) write(newsockfd, "0", 2);
            fprintf(stdout, "Exito al conectarse! Nro de descriptor de socket: %d\n", newsockfd);
            if (n < 0) {
                perror("escritura en socket");
                exit(EXIT_FAILURE);
            }
            char buffer_name[LARGE_L];
            strcpy(buffer_name, "buffer_");
            strncat(buffer_name, usuario->nombre, 35);
            buffer_name[LARGE_L - 1] = '\0';

            while(1) {
                memset(buffer, 0, TAM);
                n = unlink(strdup(buffer_name));
                if(n < 0)
                    perror("Error al remover archivo ");

                n = (int) read(newsockfd, buffer, TAM - 1);
                if (n < 0) {
                    fprintf(stderr, "Error en lectura de socket: %s\n",  strerror(errno));
                    exit(EXIT_FAILURE);
                }
                // Verificación de si hay que terminar
                buffer[strlen(buffer) - 1] = '\0';
                if (!strcmp(FIN, buffer)) {
                    printf("PROCESO %d. Como recibí %s, termino la ejecución.\n\n", getpid(), FIN);
                    close(newsockfd);
                    exit(EXIT_SUCCESS);
                }
                printf("PROCESO %d. Recibí: %s\n", getpid(), buffer);

                int numero_commando = get_numero_comando(strdup(buffer));
                if(numero_commando < 0){
                    fprintf(stderr, "Comando invalido.\n");
                    n = (int) write(newsockfd, "-1", 3);
                    if (n < 0) {
                        perror("escritura en socket");
                        exit(EXIT_FAILURE);
                    }
                } else{
                    printf("Voy a entrar al comando %d\n", numero_commando);
                    switch (numero_commando){
                        case 0:
                            r_check = exec_lisar(newsockfd, strdup(buffer_name));
                            break;
                        case 1:
                            r_check = exec_descargar_nro_estacion(newsockfd, strdup(buffer), strdup(buffer_name));
                            break;
                        case 2:
                            r_check = exec_diario_precipitacion(newsockfd, strdup(buffer), strdup(buffer_name), usuario, 0);
                            break;
                        case 3:
                            r_check = exec_diario_precipitacion(newsockfd, strdup(buffer), strdup(buffer_name), usuario, 1);
                            break;
                        case 4:
                            r_check = exec_promedio_variable(newsockfd, strdup(buffer), strdup(buffer_name));
                            break;
                        default:
                            r_check = -1;
                            break;
                    }
                    if(r_check < 0){
                        n = (int) write(newsockfd, "-1", 3);
                        fprintf(stderr, "Error en ejecucion de comando\n");
                        if (n < 0) {
                            perror("Escritura en socket");
                            fprintf(stderr, "En la linea %d de %s\n", __LINE__, __FILE__);
                            exit(EXIT_FAILURE);
                        }
                    }
                }
            }
        } else {
            close(newsockfd);
            printf("SERVIDOR: Nuevo cliente, que atiende el proceso hijo: %d\n", pid);
        }
    }
}
