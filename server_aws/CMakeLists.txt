cmake_minimum_required(VERSION 3.6)
project(cvs_parser)

set(CMAKE_C_STANDARD 11)

set(SOURCE_FILES main.c main.h commands.c commands.h parser.c parser.h server_libs.c server_libs.h commands_exec.c commands_exec.h)
add_executable(cvs_parser ${SOURCE_FILES} commands.c commands.h)