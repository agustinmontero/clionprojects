//
// Created by agustin on 31/03/17.
//

#ifndef CVS_PARSER_COMMANDS_EXEC_H
#define CVS_PARSER_COMMANDS_EXEC_H
#include <sys/socket.h>
#include <netinet/in.h>

#define UDP_PORT 6040
#define MAX_UDP_BUF_SIZE 512

int exec_lisar(int newsockfd, char *buffer_name);
int exec_descargar_nro_estacion(int newsockfd, char *buffer, char *buffer_name);
int exec_diario_precipitacion(int newsockfd, char *buffer, char *buffer_name, User *usuario, int mensual);
int exec_mensual_precipitacion(int newsockfd, struct precipitacion_diaria *precipitaciones, int s_count,
                               char *nro_estacion);
int exec_promedio_variable(int newsockfd, char *buffer, char *buffer_name);
int enviar_tamano_buffer(int newsockfd, long len);
int enviar_archivo(char* cadena, long buff_length);

#endif //CVS_PARSER_COMMANDS_EXEC_H
